import math


# Calculate the angle between two vectors in 3D space
def calculate_angle_3d(A, B, C):
    BA = (A[0] - B[0], A[1] - B[1], A[2] - B[2])
    BC = (C[0] - B[0], C[1] - B[1], C[2] - B[2])
    dot_product = BA[0] * BC[0] + BA[1] * BC[1] + BA[2] * BC[2]
    magnitude_BA = math.sqrt(BA[0]**2 + BA[1]**2 + BA[2]**2)
    magnitude_BC = math.sqrt(BC[0]**2 + BC[1]**2 + BC[2]**2)
    if magnitude_BA == 0 or magnitude_BC == 0:
        return 0  # Avoid division by zero, assume straight line
    cos_angle = dot_product / (magnitude_BA * magnitude_BC)
    # Clip cos_angle to the range [-1, 1] to avoid numerical errors
    cos_angle = max(-1, min(1, cos_angle))
    angle = math.acos(cos_angle) * (180.0 / math.pi)
    return angle