import rclpy
from rclpy.node import Node
from zed_interfaces.msg import ObjectsStamped
from perception_interfaces.srv import SitDetect
from perception_interfaces.msg import SitInfo,SitInfos
import numpy as np
from human_pose_estimation.utils import *


class HumanPoseEstimation(Node):
    def __init__(self):
        super().__init__('human_pose_estimation')
        self.subscription_skeleton = self.create_subscription(ObjectsStamped,'/zed/zed_node/body_trk/skeletons',  self.skeleton_callback,10)
        self.sit_detection_service = self.create_service(SitDetect, 'sit_detection', self.sit_detection_service)
        self.publisher_sit_infos=self.create_publisher(SitInfos,'sit_infos',10)
        # skeleton_data: [x3d,y3d,z3d,x2d,y2d]: 3d is in caemera coordinate (meter), 2d is in image coordinate (pixel)
        self.skeleton_data=[]
        self.distance_threshold=5 # meters
        self.get_logger().info("human_pose_estimation node is ready")

    def skeleton_callback(self, msg):
        skeleton_data=[]
        objects=msg.objects
        # self.get_logger().info(f"Get {len(objects)} skelenton data")
        for object in objects:
            skeleton_data_per_object=[]
            kps_3d=object.skeleton_3d.keypoints
            kps_2d=object.skeleton_2d.keypoints
            for i, kp_3d in enumerate(kps_3d[:18]):
                skeleton_data_per_object.append(np.concatenate((kp_3d.kp,kps_2d[i].kp/2)))
            if  any(np.isnan(x) for x in skeleton_data_per_object[0]):
                continue
            # Only consider the skeleton data within the distance threshold
            if np.hypot(skeleton_data_per_object[0][0], skeleton_data_per_object[0][1]) > self.distance_threshold:
                continue
            skeleton_data.append(skeleton_data_per_object)
        # self.get_logger().info(f"Get {len(skeleton_data)} skelenton data within the distance threshold")
        if len(skeleton_data)>1:
            # Sort the skeleton data by the distance of the keypoint[0] (face point of skeleton)
            skeleton_data.sort(key=lambda keypoints: np.hypot(keypoints[0][0], keypoints[0][1]))

        self.skeleton_data=skeleton_data

    def sit_detection_service(self, request, response):
        self.get_logger().info("Get resuqest for sit_detection_service")
        if self.skeleton_data ==[]:
            self.get_logger().info("Skeleton data is empty")
            response.is_detected=False
            return response
        result=[]
        for keypoint in self.skeleton_data:
            sitinfo=SitInfo()
            sitinfo.is_success, sitinfo.is_sit,sitinfo.person_position=self.sit_detection_three_points(keypoint)
            result.append(sitinfo)
        response.is_detected=True
        response.result=result

        #Publish the sit info for visualization
        msg=SitInfos()
        msg.sitinfos=result
        self.publisher_sit_infos.publish(msg)

        self.get_logger().info("Finish sit_detection_service")
        return response
        

    def sit_detection_three_points(self, keypoints):
        '''
        return: bool is_success, bool is_sit, float64[] person_position

        person_position: keypoint 0's position [x,y,z] in camera coordinate. Keypoint 0 is the face keypoint
        '''
        hip_left = keypoints[11]
        hip_right = keypoints[8]
        knee_left = keypoints[12]
        knee_right = keypoints[9]
        ankle_left = keypoints[13]
        ankle_right = keypoints[10]
        if any(np.isnan(x) for x in hip_left) or any(np.isnan(x) for x in hip_right) or any(np.isnan(x) for x in knee_left) or any(np.isnan(x) for x in knee_right) or any(np.isnan(x) for x in ankle_left) or any(np.isnan(x) for x in ankle_right):
            self.get_logger().error("3d point cloud is nan")
            return False, False,[float('nan'),float('nan'),float('nan'),float(-1),float(-1)]

        left_leg_angle = calculate_angle_3d(hip_left, knee_left, ankle_left)
        right_leg_angle = calculate_angle_3d(hip_right, knee_right, ankle_right)
        # self.get_logger().info(f"Left leg angle: {left_leg_angle}, Right leg angle: {right_leg_angle}")

        
        if left_leg_angle < 140 and right_leg_angle < 140:
            self.get_logger().info(f"Person {keypoints[0]}: sitted")
            return True, True, keypoints[0]
        else:
            self.get_logger().info(f"Person {keypoints[0]}: Not sitted")
            return True, False, keypoints[0]


def main(args=None):
    rclpy.init(args=args)

    human_pose_estimation = HumanPoseEstimation()
    rclpy.spin(human_pose_estimation)

    human_pose_estimation.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()


