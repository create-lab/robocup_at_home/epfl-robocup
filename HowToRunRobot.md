# How to run the robot?
This page is for running on the robocup laptop, jeston and rasberrypi, since there already have the environment set up in thses three devices.

# Hardware
Make sure all the thing is connected. 
Make sure copmuter also connect to the router via ethernet, and select "netplan-enp46s0" in the network setting on computer.
Note: Connect the microphone and speaker before the copmuter turn on. So it will be on the correct port as it is set up. More detail check "nlp_brain" readme in its folder.

Check the sound card: run this in your local machine
```
aplay -l
```
should be look like this:
```
card 1: QuadCast [HyperX QuadCast], device 0: USB Audio [USB Audio]
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 2: sofhdadsp [sof-hda-dsp], device 0: HDA Analog (*) []
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 2: sofhdadsp [sof-hda-dsp], device 1: HDA Digital (*) []
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 2: sofhdadsp [sof-hda-dsp], device 3: HDMI1 (*) []
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 2: sofhdadsp [sof-hda-dsp], device 4: HDMI2 (*) []
  Subdevices: 1/1
  Subdevice #0: subdevice #0
card 2: sofhdadsp [sof-hda-dsp], device 5: HDMI3 (*) []
  Subdevices: 1/1
  Subdevice #0: subdevice #0

```
- card 1 QuadCast: microphone
- card 2 sofhdadsp: speaker

Microphone need to be card1 and speaker need to be card2. This is setting in [alsa.conf](/nlp_brain/alsa.conf).

Also, check the volume is all to the maximun

```
alsamixer
```

Make sure in local machine (ex.robocup computer) the "setting" -> "sound", "sofhdadsp" could not be output device and "QuadCast" could not be input device. Otherwise, you will get error "audio open error: Device or resource busy" when running the code in docker container.

# Jeston
Open a new terminal on local computer.

Connect Jeston via ssh:
```
ssh rock@192.168.1.49
```
password: rock

Inside Jeston, we need to open two thing: one is for zed camera and one is for the object detection packages

Open the docker for zed camera first:

```
cd zed_camera

# stop the docker compose first
docker compose stop

# then replug the wired for the camera

#restart the docker compose
docker compose up
```

Note: Theroretically it is set up in a way, this docker will open automatically, however, it never open correctelly since the error zed do not detected, so tthat why we need stop the running containers first and then start it again.

Then, we open another docker for the object detection packages.

Open a new terminal on the laptop
```
# Connect to the Jeston 
ssh rock@192.168.1.49

# start docker container
docker start robocup_cv_v5

# execute the docker 
docker exec -it --user robocup robocup_cv_v5 bash

# and we can run ros command to start the node for object detecion now
ros2 run camera_control camera_control

```

# ResberryPi
Open a new terminal on local computer.

Connect RespberryPi via ssh:
```
ssh robocup@192.168.1.50
```
password: robocup

```
cd epfl-robocup/rpi

# build the docker container
./build-rpi.sh

# run the docker container
./run-rpi.sh

# run ros command to start motor contaol and lidar part
run
```

# Main computer (Robocup Laptop)
Open a new terminal on local computer.

```
# give access to docker for opening window
xhost +
```

We first run the thing for navigation
```
docker start robocup_nlp

docker exec -it --user robocup robocup_nlp bash

run_nav
```

Then we run other component (arm, face reocgnition, nlp ...)

Open a new terminal on local computer.
```
# Go into docker
docker exec -it --user robocup robocup_nlp bash

# run ros command
ros2 launch behavior_tree component.launch.py
```

Finally we can run behavior tree to start the task

Open a new terminal on local computer.
```
# Go into docker
docker exec -it --user robocup robocup_nlp bash

ros2 launch behavior_tree behavior_tree.launch.py
```





