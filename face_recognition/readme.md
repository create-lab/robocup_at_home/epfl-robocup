The work is based on [deepFace](https://github.com/serengil/deepface)

# Installation
Ubuntu 22

ROS 2 Humble

Python 3.10

deepFace
```
pip install deepface
```

perception_interfaces: a ros2 interface package for face reocgnition
```
cd ros_ws/src
git clone https://github.com/changchuntzu0618/perception_interfaces.git
```

face_recognition: git clone this repo
```
cd ros_ws/src
git clone https://gitlab.epfl.ch/create-lab/robocup_at_home/face_recognition.git
```

# Run
```
ros2 run face_recognition face_recognition_service 
```
Open another terminal
```
ros2 run face_recognition client True 
```
* add "True" to enable visualization, change to 'False' to disable visualization.