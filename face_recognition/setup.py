from setuptools import find_packages, setup
import os
from glob import glob

package_name = 'face_recognition'
submodules = 'face_recognition/functions'

setup(
    name=package_name,
    version='0.0.0',
    # packages=find_packages(exclude=['test']),
    packages=[package_name,submodules],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='root',
    maintainer_email='chun-tzu.chang@epfl.ch',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'face_recognition = face_recognition.face_recognition_main:main',
            'client = face_recognition.test_client:main',
            'pose_transform=face_recognition.pose_transform:main',
        ],
    },
)
