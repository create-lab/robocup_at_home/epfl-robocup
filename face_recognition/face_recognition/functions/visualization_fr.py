import cv2
import numpy as np

def visualize_face_recognition(img, bboxs, face_infos):
    for i, bbox in enumerate(bboxs):
        face_id=str(face_infos[i].id)
        # Extract bounding box coordinates
        x, y, w, h = bbox.x, bbox.y, bbox.w, bbox.h

        # Draw the bounding box
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # Add text ID
        cv2.putText(img, f'ID: {face_id}', (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
    
    return img

       