import json
import portalocker


def open_json(path):
    try:
        # Open the file in read mode with locking
        with open(path, 'r') as f:
            portalocker.lock(f, portalocker.LOCK_EX | portalocker.LOCK_NB)
            try:
                dic=json.load(f)
                return dic
            finally:
                portalocker.unlock(f)
    except portalocker.LockException:
        raise Exception(f"File {path} is currently locked by another process.")
    except Exception as e:
        raise Exception(f"An error occurred while opening the file: {e}")
    

def update_face_info(face_info, ppl_dic, status='old'):
    
    face_info.status = status
    
    # Update all the fields based on the corresponding dictionary values
    for key, value in ppl_dic.items():
        if hasattr(face_info, key):
            setattr(face_info, key, value)

    return face_info

def save_json(path, dic):
    try:
        # Open the file in read mode with locking
        #Save people dictionary to pkl
        with open(path, 'w') as f:
            portalocker.lock(f, portalocker.LOCK_EX | portalocker.LOCK_NB)
            try:
                json.dump(dic, f, indent=4)
            finally:
                portalocker.unlock(f)
    except portalocker.LockException:
        raise Exception(f"File {path} is currently locked by another process.")
    except Exception as e:
        raise Exception(f"An error occurred while opening the file: {e}")


def todict(obj):        
    if isinstance(obj, dict):
        return dict((key.lstrip("_"), todict(val)) for key, val in obj.items())
    elif hasattr(obj, "_ast"):
        return todict(obj._ast())
    elif hasattr(obj, "__iter__") and not isinstance(obj, str):
        return [todict(v) for v in obj]
    elif hasattr(obj, '__dict__'):
        return todict(vars(obj))
    elif hasattr(obj, '__slots__'):
        return todict(dict((name, getattr(obj, name)) for name in getattr(obj, '__slots__')))
    return obj