from .deepface_func import deepface_find
from deepface import DeepFace
import os
import cv2
import pandas as pd
import pickle
import json
from perception_interfaces.msg import FaceInfo, FaceBox
from .utils import *
import numpy as np



import rclpy
from rclpy.node import Node


from ament_index_python.packages import get_package_share_directory


package_share_directory = get_package_share_directory('face_recognition')
# Find the index of the last occurrence of 'install' in the path
index_of_install = package_share_directory.rfind('install')
# Extract everything before 'install'
ws_directory = os.path.abspath(package_share_directory[:index_of_install])
data_directory=os.path.join(ws_directory,'src/face_recognition/data')

DB_PATH=os.path.join(data_directory,"face_db")
FACE_DIC_PATH=os.path.join(data_directory,"face_dictionary.json")

MODEL_NAME= 'Facenet512'
# MODEL_NAME='VGG-Face'
DISTANCE_METRIC= 'euclidean_l2'
DETECTOR_BACKEND='opencv'

REPRESENTATION_FILE = f"representations_{MODEL_NAME}.pkl"
REPRESENTATION_FILE = REPRESENTATION_FILE.replace("-", "_").lower()

REPRESENTATION_PATH = os.path.join(DB_PATH,REPRESENTATION_FILE)


backends = [
  'opencv', 
  'ssd', 
  'dlib', 
  'mtcnn', 
  'retinaface', 
  'mediapipe',
  'yolov8',
  'yunet',
  'fastmtcnn',
]

LOGGER = rclpy.logging.get_logger("face_recognition")

def face_recognition(image,detect_time,update):
    face_infos=[]
    representations=[]
    face_bbxs=[]

    new_face_found=0
    old_face_found=0
    
    try:
        ppl_dic=open_json(FACE_DIC_PATH)
    except Exception as e:
        raise Exception(f"An error occurred while opening the file: {e}")

    # face recognition
    dfs = deepface_find(img_path = image, db_path = DB_PATH,detector_backend=DETECTOR_BACKEND, enforce_detection=False,model_name=MODEL_NAME,distance_metric=DISTANCE_METRIC,silent=True)
    LOGGER.info(f"There are {len(dfs)} face detected")

    #Only consider 3 biggest/closet faces for faster progress
    if len(dfs)>3:
        dfs=dfs[:3]

    for df in dfs:
        new_face=False
        #Initialize face info message
        face_info=FaceInfo()
        face_box=FaceBox()

        x, y, w, h = df['source_x'].values[0], df['source_y'].values[0], df['source_w'].values[0], df['source_h'].values[0]
        LOGGER.info(f"Face area :{w*h} pixels")
        face_box.x=int(x)
        face_box.y=int(y)
        face_box.w=int(w)
        face_box.h=int(h)
        # Crop the region from the image
        cropped_region = image[y:y+h, x:x+w]
        # face=cropped_region.copy()
        face=cv2.resize(cropped_region, (224, 224), interpolation = cv2.INTER_AREA)
        if df['identity'].values[0]!=None:
            old_face_found+=1
            LOGGER.info("Known Face found")
            # get all the found faces id
            found_faces=[os.path.basename(path) for path in df['identity'].values]
            found_faces_id=[int(path.split('_')[0]) for path in found_faces]
            # get the most found face id
            found_faces_id= str(max(set(found_faces_id), key=found_faces_id.count))
            
            if update:
                LOGGER.info(f'Update known found faces id {found_faces_id}')
                # get the all images names of most found face id
                found_faces_images=ppl_dic[found_faces_id]['image_names']
                # Get the number of this new face for this face id
                new_number=len(found_faces_images)
                new_image_path=str(found_faces_id)+"_"+str(new_number)+".jpg"

                # Store new face to df
                ppl_dic[found_faces_id]['image_names'].append(new_image_path)
            
                # save face to database with assign id name
                cv2.imwrite(os.path.join(DB_PATH,new_image_path),face)

            face_info=update_face_info(face_info, ppl_dic[found_faces_id], status='old')

        else:
            new_face=True
            new_face_found+=1
            LOGGER.info("New face found. Adding new face to database...")
            new_id=len(ppl_dic)
            # image name is face_id_#no.jpg
            new_image_path=str(new_id)+"_0.jpg"
            # Add new face to dictionary
            ppl_dic[new_id]={'id': str(new_id), 'image_names':[new_image_path],'name':"unknown", 'age':"unknown" ,'gender':"unknown",'race':"unknown",'emotions':"unknown", 'drinks':[]}
            
            # save face to database with assign id name
            cv2.imwrite(os.path.join(DB_PATH,new_image_path),face)
            LOGGER.info('Successfully added new face to database')

            face_info=update_face_info(face_info, ppl_dic[new_id], status='new')

        face_infos.append(face_info)
        face_bbxs.append(face_box)
        if update or new_face:
            # The first time do DeepFace.find, it will create representations_vgg_face.pkl. 
            # Add face (which add to db means every face detected) to the pkl rather than do embedding for whole db again.
            face_encoding = DeepFace.represent(img_path = face,enforce_detection = False, model_name = MODEL_NAME)
            representations.append([os.path.join(DB_PATH,new_image_path),face_encoding[0]["embedding"]])
            # representations needs to be the array with the new trained faces
   
    if dfs:
        with open(REPRESENTATION_PATH, 'rb') as f:
            representations += pickle.load(f)

        with open(REPRESENTATION_PATH, 'wb') as f:
            pickle.dump(representations, f)

        LOGGER.info(f'Found {new_face_found} new faces, Found {old_face_found} known faces')

        # Update face dictionary
        #Save people dictionary to json
        try:
            save_json(FACE_DIC_PATH,ppl_dic)
        except Exception as e:
            raise Exception(f"An error occurred while opening the file: {e}")

    return face_infos,face_bbxs


def feature_recognition(image, id):
    face_info=FaceInfo()

    # face recognition
    dfs = deepface_find(img_path = image, db_path = DB_PATH, detector_backend=DETECTOR_BACKEND,enforce_detection=False,model_name=MODEL_NAME,distance_metric=DISTANCE_METRIC,silent=True)

    if len(dfs)>0:
        df=dfs[0]

        if df['identity'].values[0]!=None:
            LOGGER.info("Known Face found")

            x, y, w, h = df['source_x'].values[0], df['source_y'].values[0], df['source_w'].values[0], df['source_h'].values[0]
            
            # Crop the region from the image
            cropped_region = image[y:y+h, x:x+w]
            face=cv2.resize(cropped_region, (224, 224), interpolation = cv2.INTER_AREA)
            
            # get all the found faces id
            found_faces=[os.path.basename(path) for path in df['identity'].values]
            found_faces_id=[int(path.split('_')[0]) for path in found_faces]
            # get the most found face id
            found_faces_id= str(max(set(found_faces_id), key=found_faces_id.count))

            if found_faces_id==id:
                try:
                    ppl_dic=open_json(FACE_DIC_PATH)
                except Exception as e:
                    raise Exception(f"An error occurred while opening the file: {e}")

                LOGGER.info("Start detecting features...")
                objs = DeepFace.analyze(img_path = face, 
                            actions = ['age', 'gender', "race", "emotion"],
                            enforce_detection = False,
                            detector_backend = 'skip',
                            silent=False,
                        )
                objes=objs[0]
                new_age=objes['age']
                new_gender=objes['dominant_gender']
                new_race=objes['dominant_race']
                new_emotion=objes['dominant_emotion']
                
                ppl_dic[id]['age']=str(new_age)
                ppl_dic[id]['gender']=str(new_gender)
                ppl_dic[id]['race']=str(new_race)
                ppl_dic[id]['emotions']=str(new_emotion)

                LOGGER.info(f"Detecting Result: Age: {new_age}, Gender: {new_gender}, Race: {new_race}, Emotion: {new_emotion}")
                

                try:
                    save_json(FACE_DIC_PATH,ppl_dic)
                except Exception as e:
                    raise Exception(f"An error occurred while opening the file: {e}")
                
                face_info=update_face_info(face_info, ppl_dic[id])


                LOGGER.info("Finish detecting features and save to database.")

                return True, face_info
            
            else:
                LOGGER.info("Detected person is not the same as provided id. Failed")
                return False, face_info
        else:
            LOGGER.info("Detected person is not the same as provided id. Failed")
            return False, face_info
    else:
        LOGGER.info("No face detected. Failed")
        return False, face_info
                


    