import os
from .functions.face_recog import face_recognition, feature_recognition
from .functions.visualization_fr import visualize_face_recognition
from perception_interfaces.srv import GetFaceRecognition, StoreNameToDb, GetPersonInfo
from perception_interfaces.msg import FaceInfo
from geometry_msgs.msg import TransformStamped
import cv2

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image,PointCloud2, CameraInfo
from cv_bridge import CvBridge

from tf2_ros import TransformBroadcaster
import numpy as np
import sensor_msgs_py.point_cloud2 as pc2
import math

import copy
from .functions.utils import *

from ament_index_python.packages import get_package_share_directory


package_share_directory = get_package_share_directory('face_recognition')
# Find the index of the last occurrence of 'install' in the path
index_of_install = package_share_directory.rfind('install')
# Extract everything before 'install'
ws_directory = os.path.abspath(package_share_directory[:index_of_install])
data_directory=os.path.join(ws_directory,'src/face_recognition/data')
FACE_DIC_PATH=os.path.join(data_directory,"face_dictionary.json")

# TODO: Add timestamp to database

class FaceRecogNode(Node):
    def __init__(self):
        super().__init__('face_recognition')

        # get the pair of coordinates of the points to search for depth
        search_list = [x for x in range(-600, 600)]
        self.search_pairs = [
            (a, b) for idx, a in enumerate(search_list) for b in search_list[idx + 1 :]
        ]
        self.search_pairs = sorted(
            self.search_pairs, key=lambda x: x[0] ** 2 + x[1] ** 2
        )

        self.representations=[]

        # self.subscription_image = self.create_subscription(Image, '/zedm/zed_node/left/image_rect_color',  self.image_callback,1)
        self.subscription_image = self.create_subscription(Image, 'zed_left_image',  self.image_callback,1)
        
        self.srv_get_face_recognition = self.create_service(GetFaceRecognition, 'get_face_recognition', self.get_face_recognition_callback)
        # # self.get_logger().info('Service /get_face_recognition is ready.')

        self.srv_get_feature_recognition = self.create_service(GetPersonInfo, 'get_feature_recognition', self.get_feature_recognition_callback)

        self.srv_store_name_to_db = self.create_service(StoreNameToDb, 'store_name_to_db', self.store_name_to_db_callback)
        # # self.get_logger().info('Service /store_name_to_db is ready.')

        self.srv_store_drink_to_db = self.create_service(StoreNameToDb, 'store_drink_to_db', self.store_drink_to_db_callback)

        # # A service: given a person id, return the person's information
        self.srv_get_person_info = self.create_service(GetPersonInfo, 'get_person_info', self.get_person_info_callback)

        # # subscribes point cloud topic from zed-ros2, ~/point_cloud/cloud_registered: Registered color point cloud.
        # self.subscription_point_cloud = self.create_subscription(PointCloud2,'/zedm/zed_node/point_cloud/cloud_registered', self.point_cloud_callback,10)
        self.subscription_point_cloud = self.create_subscription(PointCloud2,'zed_point_cloud', self.point_cloud_callback,10)

        # # subscribes camerainfo topic from zed-ros2, ~/left/camera_info: Left camera calibration data.
        self.subscription_camera_info = self.create_subscription(CameraInfo,'/zedm/zed_node/left/camera_info', self.camera_info_callback, 10)

        self.publisher_face_image = self.create_publisher(Image, 'visualization_face_recognition', 10)

        self.tf_broadcaster = TransformBroadcaster(self)

        self.cv_bridge = CvBridge()
        self.last_time = self.get_clock().now()
        self.get_logger().info("Face recognition node is ready.")

        

    def image_callback(self, msg):
        # self.get_logger().info("Received an image!")
        self.image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
    
    def box_to_middle_point(self,box):
        return ((box.x+box.w/2),(box.y+box.h/2))
        
    
    def get_face_recognition_callback(self, request, response):
        self.get_logger().info("Received a request! Start face recognition.")

        self.current_time = self.get_clock().now()
        # Calculate the time difference in seconds
        time_difference = (self.current_time - self.last_time).nanoseconds / 1e9
        # if time_difference larger than 1 second, update the face database
        if time_difference >= 2.0:
            self.update=True
            self.last_time = copy.copy(self.current_time)
        else:
            self.update=False

        # Do face recognition
        try:
            self.face_infos, self.face_bbxs=face_recognition(self.image,self.current_time,self.update)
        except Exception as e:
            self.get_logger().error(f"An error occurred while doing face recognition: {e}")
            return response

        response.face_info=self.face_infos
        response.face_bbx=self.face_bbxs
        
        # get face transform and store face transform in the database
        face_points=[self.box_to_middle_point(x) for x in self.face_bbxs]
        face_transforms=[self.get_face_pose(x,info) for x,info in zip(face_points,self.face_infos)]
        response.face_transform = copy.copy(face_transforms)
        try:
            ppl_dic=open_json(FACE_DIC_PATH)
        except Exception as e:
            self.get_logger().error(f"An error occurred while opening the file: {e}")

        for info, face_transform in zip(self.face_infos, face_transforms):
            if info.id in ppl_dic:
                ppl_dic[info.id]['face_transform']=todict(face_transform)
        try:
            save_json(FACE_DIC_PATH, ppl_dic)
        except Exception as e:
            self.get_logger().error(f"An error occurred while opening the file: {e}")

        self.get_logger().info("Finished face recognition.")
        
        # Publish the image with face recognition visualization
        visualize_image=visualize_face_recognition(self.image, self.face_bbxs, self.face_infos)
        self.publisher_face_image.publish(self.cv_bridge.cv2_to_imgmsg(visualize_image, encoding="bgr8"))
        
        return response
    
    def get_feature_recognition_callback(self,request, response):
        self.get_logger().info("Received a request! Start feature recognition.")
        # Do feature recognition
        try:
            success,face_info=feature_recognition(self.image,request.id)
            response.face_info=face_info
            response.success=success

        except Exception as e:
            self.get_logger().error(f"An error occurred while doing feature recognition: {e}")
            response.face_info=FaceInfo()
            response.success=False
        
        return response
    
    def store_name_to_db_callback(self, request, response):
        self.get_logger().info("Received a request! Start storing name to database.")
        #if it is name, it should only contain one name
        name = request.names[0]
        id = request.id
        self.get_logger().info(f"Person ID: {id} ,Name: {name}")

        try:
            ppl_dic=open_json(FACE_DIC_PATH)
            ppl_dic[id]['name']=name
            save_json(FACE_DIC_PATH,ppl_dic)
            self.get_logger().info("Finished storing name to database.")
            response.success=True
        except Exception as e:
            self.get_logger().info(f"Failed to store name to database. {e}.")
            response.success=False
        return response
    
    def store_drink_to_db_callback(self, request, response):
        self.get_logger().info("Received a request! Start storing drink to database.")
        name = request.names
        id = request.id
        self.get_logger().info(f"Person ID: {id} ,Drink name: {name}")

        try:
            ppl_dic=open_json(FACE_DIC_PATH)
            ppl_dic[id]['drinks']=name
            save_json(FACE_DIC_PATH,ppl_dic)
            self.get_logger().info("Finished storing drink to database.")
            response.success=True
        except Exception as e:
            self.get_logger().info(f"Failed to store drink to database. {e}")
            response.success=False
        return response
    
    def get_person_info_callback(self, request, response):
        self.get_logger().info("Received a request! Start getting person information.")
        id = request.id

        try:
            ppl_dic=open_json(FACE_DIC_PATH)
            face_info=FaceInfo()
            face_info=update_face_info(face_info,ppl_dic[id])
            self.get_logger().info("Finished getting person information.")
            response.face_info=face_info
            response.success=True

        except Exception as e:
            self.get_logger().info(f"Failed to get person information. {e}")
            response.face_info=FaceInfo()
            response.success=False
            
        return response
    
    def get_face_pose(self, point, info):
        self.get_logger().info(f"point in camera: {point}")
        try:
            x_3d,y_3d,z_3d=self.get_point3D(int(point[0]),int(point[1]))
        except Exception as e:
            self.get_logger().info(f"Failed to get 3D point. {e}")
            return TransformStamped()

        self.get_logger().info(f"point 3d: {x_3d},{y_3d},{z_3d}")

        t = TransformStamped()
        t.header.stamp = self.get_clock().now().to_msg()
        t.header.frame_id = "camera"
        t.child_frame_id = "face_"+str(info.id)

        # no rotation for the object (it is just seen as a point)
        t.transform.rotation.x = 0.0
        t.transform.rotation.y = 0.0
        t.transform.rotation.z = 0.0
        t.transform.rotation.w = 1.0

        # object coordinates
        t.transform.translation.x = float(x_3d)
        t.transform.translation.y = float(y_3d)
        t.transform.translation.z = float(z_3d)

        # Send the transformation
        self.tf_broadcaster.sendTransform(t)

        return t
    
    def camera_info_callback(self, msg):
        # rclpy.logging.get_logger("camera_control").info("received camera info")
        # update camera_info in camera 
        self.camera_info = msg
        self.image_size = {'width':self.camera_info.width, 'height':self.camera_info.height}
    
    def point_cloud_callback(self, msg):
        # update point_cloud in camera 
        self.point_cloud = np.array(pc2.read_points_numpy(msg, field_names=['x', 'y', 'z'], skip_nans=False,reshape_organized_cloud=False)).reshape((360,640,3))

    def get_point3D(self, x: int, y: int):
        """Gets the 3D point value at the specified coordinates of the image.

        Parameters
        ----------
        x : int
            The x coordinate of the point of interest in the image.
        y : int
            The y coordinate of the point of interest in the image.

        Returns
        -------
        xf : float
            The x coordinate in meters of the 3D point.
        yf : float
            The y coordinate in meters of the 3D point.
        zf : float
            The z coordinate in meters of the 3D point.

        Raises
        ------
        Exception
            If the point cloud access failed.
        """
        if self.point_cloud is not None:
            # # Retrieve the point cloud aligned on the left eye of the camera (the one close to the ZED Mini logo)
            # # NOTICE: coordinate is ROS coordinate

            i = 0
            xd = 0
            yd = 0
            
            point3D = self.point_cloud[y, x]
            xf = point3D[0]
            yf = point3D[1]
            zf = point3D[2]
            while (self.is_nan(xf) or self.is_nan(yf) or self.is_nan(zf)):

                (xd, yd) = self.search_pairs[i]
                i += 1

                if i >= len(self.search_pairs):
                    raise Exception("Point cloud access failed after trying all pairs")

                while (
                    x + xd < 0
                    or x + xd >= self.image_size['width']
                    or y + yd < 0
                    or y + yd >= self.image_size['height']
                ):
                    (xd, yd) = self.search_pairs[i]
                    i += 1

                    if i >= len(self.search_pairs):
                        raise Exception(
                            "Point cloud access failed after trying all pairs"
                        )
                # TODO: check if this is the correct way to access the point cloud map
                point3D = self.point_cloud[y, x]

                xf = point3D[0]
                yf = point3D[1]
                zf = point3D[2]

            return xf, yf, zf
        else:
            raise Exception("Point cloud access failed")
    
    def is_nan(self, x: float) -> bool:
        """Checks if the value is NaN.

        Parameters
        ----------
        x : float
            The value to check.

        Returns
        -------
        bool
            True if the value is NaN, False otherwise.

        """
        return (
            x == float("inf")
            or x == float("-inf")
            or math.isnan(x)
            or (math.isinf(x) and x > 0)
            or (math.isinf(x) and x < 0)
            or np.isinf(x)
            or np.isnan(x)
        )
    
    

        

def main(args=None):
    rclpy.init(args=args)

    face_recognition_node = FaceRecogNode()

    rclpy.spin(face_recognition_node)

    face_recognition_node.destroy_node()
    rclpy.shutdown()
    print("Face recognition node has been shut down.")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
