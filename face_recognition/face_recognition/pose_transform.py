import rclpy
from rclpy.node import Node
import tf2_ros
from perception_interfaces.srv import GetFaceRecognition
import cv2
import sys
from cv_bridge import CvBridge

class Pose_transform(Node):
    
    def __init__(self):
        super().__init__('pose_transform')
        self.tfBuffer = tf2_ros.Buffer()
        self.tfListener = tf2_ros.TransformListener(self.tfBuffer, self)
        self.facerecognition_client = self.create_client(GetFaceRecognition, 'get_face_recognition')
        while not self.facerecognition_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')

        # self.timer_facerecognition_client = self.create_timer(2, self.timer_callback_facerecognition_client)

        self.req = GetFaceRecognition.Request()


    
    def send_request(self,visualization):
        #ask for face recognition
        self.req.visualization = visualization=='True'
        self.future = self.facerecognition_client.call_async(self.req)
        rclpy.spin_until_future_complete(self, self.future)
        # print('Waiting for response')
        response=self.future.result()
        self.get_logger().info('Result:' + str(response.face_transform))
        # get the poses of the face
        poses=response.face_transform

        for pose in poses:
            from_frame_rel=pose.child_frame_id
            to_frame_rel="robot_base"

            t = self.tfBuffer.lookup_transform(
                    to_frame_rel,
                    from_frame_rel,
                    rclpy.time.Time())
            # print all the values in t.transform.translation
            print(f't.transform.translation.x:{t.transform.translation.x},t.transform.translation.y:{t.transform.translation.y},t.transform.translation.z:{t.transform.translation.z}')
            print(f't.transform.rotation.x:{t.transform.rotation.x},t.transform.rotation.y:{t.transform.rotation.y},t.transform.rotation.z:{t.transform.rotation.z},t.transform.rotation.w:{t.transform.rotation.w}')
        
        return response
            
   
        

def main(args=None):
    rclpy.init(args=args)
    node = Pose_transform()
    cv_bridge = CvBridge()

    try:
        while rclpy.ok():
            response = node.send_request(str(sys.argv[1]))
            # node.get_logger().info(
            #     'Result:' + str(response.face_transform))
            if str(sys.argv[1]) == 'True' and response.face_image.data:
                face_result=cv_bridge.imgmsg_to_cv2(response.face_image, desired_encoding='bgr8')
                cv2.imshow('Face Recognition', face_result)
                cv2.waitKey(1)
            
    except KeyboardInterrupt:
        pass

    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()