import rclpy
from rclpy.node import Node
from scipy.spatial.transform import Rotation as R
from arm_interfaces.srv import ArmCommand
import numpy as np
from tf2_ros import TransformBroadcaster
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from .tf_stuff import broadcast_tf, tf2_to_arm_cartesian_description, tcp_to_camera_transform, arm_cartesian_description_to_tf2
from xarm.wrapper import XArmAPI

class XAarmContorlNode(Node):

    def __init__(self):
        super().__init__('XArm_control_node')

        # tf related
        self.tf_broadcaster = TransformBroadcaster(self)
        self.tf_buffer = Buffer(cache_time=rclpy.duration.Duration(seconds=3))
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.mainloop = self.create_timer(0.05, self.tf_callback)

        # ARM
        self.arm = XArmAPI('192.168.1.234')
        self.arm.connect()

        # PARA
        self.camera_pitch = 0
        self.gripper_length = 0.16

        self.current_pose = None
        self.target_pose = None

        self.arm.motion_enable(enable=True)
        self.arm.set_mode(0)
        self.arm.set_state(state=0)
        self.current_pose = self.arm.get_position()[1]

        self.move_joint = self.create_service(ArmCommand, 'move_joint', self.move_joint)
        self.set_target = self.create_service(ArmCommand, 'set_target', self.set_target)
        self.move_pose = self.create_service(ArmCommand, 'move_pose', self.move_pose)

    def set_target(self, request, response):
        cmd = request.command
        pose = request.data

        self.target_pose = pose

        response.success = True
        return response

    def move_pose(self, request, response):
        cmd = request.command
        pose = request.data

        try:
            t = self.tf_buffer.lookup_transform("arm_base", "arm_target", rclpy.time.Time())
        except:
            response.success = False
            return response
        self.current_pose = self.arm.get_position()[1]

        target_trans_temp = np.array([t.transform.translation.x, t.transform.translation.y, t.transform.translation.z])
        position, quat = arm_cartesian_description_to_tf2(self.current_pose)
        current_trans = np.asarray(position)

        diff = target_trans_temp - current_trans
        norm_diff = diff / np.linalg.norm(diff)
        true_diff = diff - self.gripper_length * norm_diff

        target_trans = current_trans + true_diff

        target_pose = [target_trans[0], target_trans[1], target_trans[2], 0, 0, 0, 1]
        
        target_pose = tf2_to_arm_cartesian_description(target_pose)

        self.arm.set_position(x=target_pose[0], y=target_pose[1], z=target_pose[2], 
                              roll=self.current_pose[3], pitch=self.current_pose[4], yaw=self.current_pose[5], 
                              speed=40, wait=True)

        response.success = True
        return response

    def move_joint(self, request, response):
        cmd = request.command
        joint = request.data

        print("joint:", joint)
        print("cmd:", cmd)

        target = []
        if "relative" in cmd:
            # change from radian to degree
            curr_angles = self.arm.angles
            print("curr_angles:", curr_angles)
        else:
            curr_angles = [0, 0, 0, 0, 0, 0]

        for i in range(6):
            target.append(float(curr_angles[i] + joint[i]))
        

        # for not breaking computer
        if target[0] > 100:
            target[0] = 100
        if target[0] < -100:
            target[0] = -100
        
        print("target:", target)

        if self.check_angle_limits(target):
            self.arm.set_servo_angle(angle=target, speed=10, wait=True)
            self.current_pose = self.arm.get_position()[1]


            quat = R.from_euler("xyz", [self.current_pose[3], self.current_pose[4], self.current_pose[5]], degrees = True).as_quat()
            print(self.current_pose[:3], quat)

            response.success = True
        else:
            print("Joint limits exceeded")
            response.success = False

        return response

    def check_angle_limits(self, angle_demand):
        is_safe = True
        # if angle_demand[0] > 350 or angle_demand[0] < -350:
        #     is_safe = False
        # if angle_demand[1] > 120 or angle_demand[1] < -120:
        #     is_safe = False
        # if angle_demand[2] > 0 or angle_demand[2] < -225:
        #     is_safe = False
        # if angle_demand[3] > 350 or angle_demand[3] < -350:
        #     is_safe = False
        # if angle_demand[4] > 350 or angle_demand[4] < -350:
        #     is_safe = False
        # if angle_demand[5] > 175 or angle_demand[5] < -95:
        #     is_safe = False
        return is_safe

    def tf_callback(self):
        if self.current_pose is None:
            return
        
        position, quat = arm_cartesian_description_to_tf2(self.current_pose)
        broadcast_tf("arm_base", "arm_tcp", position, quat, self.tf_broadcaster, self.get_clock().now().to_msg())

        broadcast_tf("base_link", "arm_base", [0.11,0.0,0.7], [0.0,0.0,0.0,1.0], self.tf_broadcaster, self.get_clock().now().to_msg())
        
        cam_rel_pose = tcp_to_camera_transform(pitch = self.camera_pitch)
        broadcast_tf("arm_tcp", "camera", cam_rel_pose[:3], cam_rel_pose[3:], self.tf_broadcaster, self.get_clock().now().to_msg())

        if self.target_pose is not None:
            broadcast_tf("camera", "arm_target", self.target_pose[:3], self.target_pose[3:], self.tf_broadcaster, self.get_clock().now().to_msg())

def main(args=None):
    rclpy.init(args=args)

    XArm_control_node = XAarmContorlNode()

    rclpy.spin(XArm_control_node)

    XArm_control_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()