import rclpy
from rclpy.node import Node
from custom_services.srv import ArmCommand
import numpy as np
from .dynamixel_controller import Dynamixel
import time

class GripperControllerNode(Node):

    def __init__(self):
        super().__init__('gripper_controller_node')

        # Dyanamixel
        self.gripper = Dynamixel(1, "Gripper", "/dev/ttyUSB0", 2000000, "xm")
        self.gripper.begin_communication()
        self.gripper.set_operating_mode("current-based position")
        self.gripper.write_profile_velocity(30)
        
        # PARA
        self.open = 1580
        self.close = 150

        self.move_gripper = self.create_service(ArmCommand, 'move_gripper', self.move_gripper)

    def move_gripper(self, request, response):
        cmd = request.command
        current = request.data[0]

        if "open" in cmd:
            self.gripper.write_current(300)
            self.gripper.write_position(self.open)
        else:
            self.gripper.write_current(current)
            self.gripper.write_position(self.close)

        time.sleep(2)
        response.success = True

        return response
        

def main(args=None):
    rclpy.init(args=args)

    gripper_controller_node = GripperControllerNode()

    rclpy.spin(gripper_controller_node)

    gripper_controller_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()