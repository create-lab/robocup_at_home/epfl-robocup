from scipy.spatial.transform import Rotation as R
from geometry_msgs.msg import TransformStamped
from tf2_ros import TransformBroadcaster
import numpy as np

def get_series_quaternion(q1, q2):
    r1 = R.from_quat(q1)
    r2 = R.from_quat(q2)
            
    r3 = r1 * r2

    return r3.as_quat()

def tcp_to_camera_transform(pitch):
    pitch_angle = np.radians(pitch)
    camera_joint_offset = [0.075 - 0.04 * np.sin(pitch_angle), -0.025, 0.08 + 0.04 * np.cos(pitch_angle)]
    
    pitch_q = R.from_euler("Y", -pitch_angle)
    # rot1 and rot2 defines arm -> camera frame orientation
    rot1 = R.from_euler("Z", 180, degrees=True)
    rot2 = R.from_euler("Y", -90, degrees=True)

    q_total = list((pitch_q * rot1 * rot2).as_quat())

    relative_pose = camera_joint_offset + q_total

    return relative_pose


def arm_cartesian_description_to_tf2(pose):
    quat = R.from_euler("xyz", [pose[3], pose[4], pose[5]], degrees = True).as_quat()
    position = [pose[0]/1000, pose[1]/1000, pose[2]/1000]

    return position, quat
        
def tf2_to_arm_cartesian_description(pose):
    target = []
    target.append(float(pose[0]*1000))
    target.append(float(pose[1]*1000))
    target.append(float(pose[2]*1000))
    
    euler = R.from_quat(pose[3:]).as_euler("xyz", degrees = True)

    for i in range(3):
        target.append(euler[i])

    return target

def broadcast_tf(base_name, frame_name, position, quaternion, tf_broadcaster:TransformBroadcaster, timestamp):
    t = TransformStamped()
    t.header.stamp = timestamp
    t.header.frame_id = base_name
    t.child_frame_id = frame_name

    t.transform.translation.x = position[0]
    t.transform.translation.y = position[1]
    t.transform.translation.z = position[2]

    t.transform.rotation.x = quaternion[0]
    t.transform.rotation.y = quaternion[1]
    t.transform.rotation.z = quaternion[2]
    t.transform.rotation.w = quaternion[3]

    tf_broadcaster.sendTransform(t)
    
    return t
