from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    xarm_controller_node = Node(
        package="xarm_control",
        executable="xarm_control_node",
        name="xarm_control_node",
        output="screen",
    )
    gripper_controller_node = Node(
        package="xarm_control",
        executable="gripper_control_node",
        name="gripper_control_node",
        output="screen",
    )

    return LaunchDescription([
        gripper_controller_node,  
        xarm_controller_node
     ])