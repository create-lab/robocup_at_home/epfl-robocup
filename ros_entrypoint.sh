#!/bin/bash

export PATH="$PATH:/home/robocup/.local/bin"
export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp
echo "RMW_IMPLEMENTATION=$RMW_IMPLEMENTATION"

# setup ros environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
source "/home/robocup/ros2_ws/install/setup.bash"

cd ~/ros2_ws

exec "$@"