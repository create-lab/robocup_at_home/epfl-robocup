from vosk import Model, KaldiRecognizer
import time

import rclpy
from rclpy.node import Node
from nlp_brain_interface.srv import BrainCmd, Say
from nlp_brain_interface.action import GetName
from nlp_brain.nlp_model.chatbot import ChatBot
from std_msgs.msg import String
from rclpy.executors import MultiThreadedExecutor
from rclpy.callback_groups import ReentrantCallbackGroup
from rclpy.action import ActionServer

import pyttsx3


class NLP(Node):
    """
    This class implements the Natural Language Processing (NLP) module.
    """

    def __init__(self):

        # Chatbot
        self.chatbot = ChatBot()
        
        
        # ROS parameters
        super().__init__('nlp_node')
        self.logger= self.get_logger()
        # Allow callbacks to be executed in parallel without restriction.
        self.group = ReentrantCallbackGroup()

        # ROS 
        # an action server to ask the user for their name or favorite drink
        self._action_server = ActionServer(self,GetName,'ask_name_or_drink',self.ask_name_or_drink_callback,callback_group=self.group,goal_callback=self.goal_callback_ask_name_or_drink)
        # a service to force the robot to say something
        self.srv_say= self.create_service(Say, 'say', self.say_callback,callback_group=self.group)
        # a subscriber to subscribe to the recognized text
        self.sub_stt = self.create_subscription(String, 'stt', self.stt_callback, 1,callback_group=self.group)

        #Set up
        self.stop_speaking_say= False
        self.stop_speaking_ask= False
        self.stop_listen = False
        self.heard_text= None

        self.logger.info("Correctly initialized NLP node")

    #########################################################################################################
    # ------------------------------------------ # TTS METHODS # ------------------------------------------ #
    #########################################################################################################
    
    def text_to_speech(self, text: str):
        """
        Converts text to speech and output the sound.
        
        Args:
            text: The text to convert to speech.
        """
        if text=="" or text is None:
            self.logger.info("Text is empty, nothing to say...")
            return
        
        self.logger.info(f'Speaking: {text}')
        self.stop_listen = True

        #using espeakng: https://github.com/sayak-brm/espeakng-python
        '''
        mySpeaker = espeakng.Speaker()
        mySpeaker.wpm=140
        mySpeaker.say(text)
        '''
        

        #using pyttsx3, offline
        engine = pyttsx3.init("espeak")
        rate=engine.getProperty('rate')
        engine.setProperty('rate', rate-40)
        engine.setProperty('voice',"english-us")
        engine.say(text)
        engine.runAndWait()


        #using gTTS but it need internet
        '''
        # Generate speech and save to a BytesIO object
        mp3_fp = BytesIO()
        tts = gTTS(text, lang='en')
        tts.write_to_fp(mp3_fp)

        # convert mp3 to wav
        mp3_fp.seek(0)
        mp3_audio = AudioSegment.from_file(mp3_fp, format="mp3")
        wav_io = BytesIO()
        mp3_audio.export(wav_io, format="wav")
        wav_io.seek(0)
        audio = AudioSegment.from_file(wav_io, format="wav")

        # Play the audio
        play(audio)
        '''
        # self.logger.info(f"Finsihed saying...")
        time.sleep(0.2)
        self.stop_listen = False

    #########################################################################################################
    # ----------------------------------------- # Chatbot # ------------------------------------------------#
    #########################################################################################################     

    def generate_response(self, user_input: str):
        """
        Generates a response to the user input.
        
        Args:
            user_input: The user input.
        Returns:
            conversation_response: The response to the user input.
            name: The person names extracted from the user input. Defualt is None.
            drink: The drink names extracted from the user input. Defualt is None.
        """
        conversation_response= self.chatbot.generate_response(user_input)
        behavior_category = self.chatbot.behavior_category_classifier(user_input)
        self.logger.info(f"User: {user_input} ({behavior_category})")
        self.logger.info(f"Robot: {conversation_response}")

        name = None
        drink = None

        if "introducing him/her with name" in behavior_category:
            name= self.chatbot.name_extractor(user_input)
            self.logger.info(f"Name: {name}")

        if "tell favorite drink" in behavior_category:
            drink= self.chatbot.drink_extractor(user_input)
            self.logger.info(f"Favorite drink: {drink}")

        return conversation_response, name, drink
    

    #########################################################################################################
    # ----------------------------------------- # ROS # ----------------------------------------------------#
    #########################################################################################################   

    def ask_name_or_drink_callback(self, goal_handle):
        """
        Callback method for the ask_name_or_drink action.
        The robot will ask the user for their name or favorite drink, and then return person names or dirnk names.

        GetName.action:
            string question #given question
            string ask_for  #name or drink
            ---
            string[] names #return names or drink names
            ---
            string feedback #middle feedback, "waiting for response" or "heard response"
        """
        request= goal_handle.request
        feedback_msg = GetName.Feedback()
        self.logger.info(f"Get request for {request.ask_for}")
        self.stop_speaking_ask= True

        self.heard_text= None
        question= request.question
        self.text_to_speech(question)
        
        while self.heard_text is None:
            feedback_msg.feedback= "waiting for response"
            goal_handle.publish_feedback(feedback_msg)
            time.sleep(0.1)
        
        feedback_msg.feedback= "heard response"
        goal_handle.publish_feedback(feedback_msg)

        goal_handle.succeed()
        response_text, name, drink= self.generate_response(self.heard_text)
        # self.text_to_speech(response_text)

        result = GetName.Result()
        if request.ask_for=="name" and name is not None:
            result.names= name
        elif request.ask_for=="drink" and drink is not None:
            result.names= drink
        else:
            result.names= []
        
        self.stop_speaking_ask= False
        self.heard_text= None

        return result
    
    def goal_callback_ask_name_or_drink(self, goal_request):
        """
        Callback method for the ask_name_or_drink action goal request.
        """
        self.logger.info("send goal response accept")
        return rclpy.action.GoalResponse.ACCEPT
    
    def say_callback(self, request, response):
        """
        Callback method for the say service.
        The robot will say the text provided in the request.
        """
        self.stop_speaking_say= True
        text= request.text
        self.text_to_speech(text)
        self.stop_speaking_say= False

        return response

    def stt_callback(self, msg):
        """
        Callback method for the recognized text subscriber.
        """
        if not self.stop_listen:
            self.heard_text= msg.data

            # if (not self.stop_speaking_ask) and (not self.stop_speaking_say):
            #     response_text, _ , _ = self.generate_response(self.heard_text)
            #     self.text_to_speech(response_text)
            #     self.heard_text= None

    

def main():
    rclpy.init()

    client = NLP()
    executor = MultiThreadedExecutor(num_threads=2)
    executor.add_node(client)
    executor.spin()

    rclpy.shutdown()


if __name__ == '__main__':
    main()