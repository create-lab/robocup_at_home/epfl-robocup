from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, pipeline
import torch
import ast
import rclpy

class ChatBot():
    def __init__(self) -> None:
        self.chat = [
            {"role": "system", "content": """
            You are a service robot in a competition, so please give short and precise response!
            """
            }
        ]

        # set up LLM: using Meta-Llama-3-8B-Instruct with 4-bit quantization.
        # *If it is first time download model, need huggingface access token with access of the model, need to ask on the model's huggingface website
        access_token=None
        quantization_config = BitsAndBytesConfig(load_in_4bit=True)
        self.pipe = pipeline("text-generation", "meta-llama/Meta-Llama-3-8B-Instruct",token=access_token, torch_dtype=torch.bfloat16, device_map="auto", model_kwargs={"quantization_config": quantization_config})
        self.max_new_tokens = 128
        self.logger=rclpy.logging.get_logger('ChatBot')
        self.logger.info("Chatbot is ready!")

    def generate_response(self, user_input: str) -> str:
        self.chat.append(
            {"role": "user", "content": user_input}
        )
        response = self.pipe(self.chat, max_new_tokens=self.max_new_tokens)
        conversation_response = response[0]['generated_text'][-1]['content']
        self.chat = response[0]['generated_text']
        
        return conversation_response
    
    def behavior_category_classifier(self, user_input: str) -> str:
        behavior_classifier_chat = [
            {"role": "system", "content": """
                You are a helpful assistant. Please classify the user content to the following "behavior category":
                1. introducing him/her with name
                2. tell favorite drink
                3. other
                
                please classify the user content to one of the category in the list "behavior category" above. 
                It could contain more than one category. Response ONLY WITH category.
            
                For example:
                Input: my name is jessica
                Output: ["introducing him/her with name"]
            
                Input: I want to drink mojito
                Output: ["tell favorite drink"]
            
                Input: Hi
                Output: ["other"]
            
                Input: Hi My name is jessica. I want to drink mojito
                Output: ["introducing him/her with name", "tell favorite drink"]
            
            """
            },
            {"role": "user", "content": user_input}
        ]
        response = self.pipe(behavior_classifier_chat, max_new_tokens=256)
        response_text = response[0]['generated_text'][-1]['content']
        try:
            result_categories= ast.literal_eval(str(response_text))
        except:
            result_categories= ['other']

        return result_categories
    
    def name_extractor(self, user_input: str) -> str:
        
        chat_name_extractor = [
            {"role": "system", "content": """
                Please extract the person's name from the user input.
                Please ONLY return names in list format. Do not return any explaination or other sentences. 
                Name list: Sophie, Julia, Emma, Sara, Laura, Hayley, Susan, Fleur, Gabrielle, Robin, John, Liam, Lucas, William, Kevin, Jesse, Noah, Harrie, Peter
                The name should be inside the name list. Please return the name in the name list and closet to the user input.
                If there is no name, return an empty list, eg.[].
                Please only return one name.
            
                For example:
                Example 1:
                Input: Hi My name is Susan
                Output: ['susan']
            
                Example 2:
                Input: Hi
                Output: []
            
            """
            },
            {"role": "user", "content": user_input}]
        
        response = self.pipe(chat_name_extractor, max_new_tokens=256)
        response_text = response[0]['generated_text'][-1]['content']
        try:
            response_names= ast.literal_eval(str(response_text))
        except:
            response_names= []

        result_names=[name for name in response_names if self.name_checker(str(name))]
        
        return result_names
    
    def name_checker(self,name: str) -> bool:
        chat_name_checker = [
            {"role": "system", "content": """
                You are a content checker.
                Check the user content is a person's name or not. And check the name is in the name list or not.
                Name list: Sophie, Julia, Emma, Sara, Laura, Hayley, Susan, Fleur, Gabrielle, Robin, John, Liam, Lucas, William, Kevin, Jesse, Noah, Harrie, Peter
                Return True if it is a person's name and in the name list, otherwise return False.
                Definition of person's name:
                a name (as the praenomen or the forename) by which an individual is intimately known or designated and which may be displaced or supplemented by a surname, a cognomen, or a royal name
                For example:
                Input: Gabrielle
                Output: True
                Input: jessica
                Output: False
                Input: name
                Output: False
                Input: Emma
                Output: True
            """
            },
            {"role": "user", "content": name}
        ]
        response = self.pipe(chat_name_checker, max_new_tokens=256)
        response_text = response[0]['generated_text'][-1]['content']
        name_check= self.strtobool(response_text)
        # name_check= ast.literal_eval(response_text)
        # print(f'{name}:{name_check}')

        return name_check
    
    def drink_extractor(self, user_input: str) -> str:
        chat_drink_extractor = [
            {"role": "system", "content": """
                You are a helpful assistant. Please extract the drink name from the user input.
                Please ONLY return drink names in list format. Do not return any explaination or other sentences.
                Drink List: cola, ice tea, water, milk, big coke, fanta, dubbelfris
                The name should be inside the drink list. Please return the drink in the drink list and closet to the user input.
                If there is no drink name, return an empty list, eg.[].
                Do not create the drink name. Only extract the drink name from the user input.
            
                For example:
                Input: My favorite dinrk is apple juice
                Output: ['apple juice']
            
                Input: Hi
                Output: []
            
            """
            },
            {"role": "user", "content": user_input}
        ]

        response = self.pipe(chat_drink_extractor, max_new_tokens=256)
        response_text = response[0]['generated_text'][-1]['content']
        try:
            response_drinks= ast.literal_eval(str(response_text))
        except:
            response_drinks= []

        result_drinks=[drink_name for drink_name in response_drinks if self.drink_checker(str(drink_name))]

        return result_drinks
    
    def drink_checker(self,drink_name: str) -> bool:
        chat_drink_checker = [
            {"role": "system", "content": """
                You are a content checker.
                Check the user content is a drink's name or not. 
                Drink List: cola, ice tea, water, milk, big coke, fanta, dubbelfris
                Return True if it is a drink's name and inside the drink list, otherwise return False.
        
                For example:
                Input: cola
                Output: True
                Input: dubbelfris
                Output: True
                Input: name
                Output: False
                Input: apple juice
                Output: False
            """
            },
            {"role": "user", "content": drink_name}
        ]
        response = self.pipe(chat_drink_checker, max_new_tokens=256)
        response_text = response[0]['generated_text'][-1]['content']
        drink_check= self.strtobool(response_text)
        # drink_check= ast.literal_eval(response_text)
        # print(f'{drink_name}:{drink_check}')

        return drink_check
    
    def strtobool (self,val):

        val = val.lower()
        if val in ('y', 'yes', 't', 'true', 'on', '1'):
            return 1
        elif val in ('n', 'no', 'f', 'false', 'off', '0'):
            return 0
        else:
            return 0