import pyaudio
from vosk import Model, KaldiRecognizer
import time
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
import ast


class STT(Node):
    """
    This class implements speech to text, provide a publisher which publish recognized text, using ROS2.
    """

    def __init__(self,
                 
                 # AUDIO
                 audio_model_path = r"src/nlp_brain/nlp_brain/vosk-model-small-en-us-0.15",
                 audio_sample_rate = 16000,
                 audio_channels = 1,
                 audio_chunck_size = 1024,
                 audio_listening_time = 3,
                 
                 ):
        """
        Constructor method to initialize both public and private instance-specific attributes.
        
        Args:
            - AUDIO -
            audio_model_path: Path to the model used by the listener.download from: https://alphacephei.com/vosk/models
            audio_sample_rate: Sample rate of the audio signal.
            audio_channels: Number of channels of the audio signal.
            audio_chunck_size: Size of the audio signal chuncks.
            audio_listening_time: Maximum time to listen to the audio signal.
            
        """
         # ROS parameters
        super().__init__('stt_node')
        self.logger= self.get_logger()

        # Audio parameters
        self.logger.info("Setting audio parameters...")
        self.audio_model_path = audio_model_path
        self.audio_sample_rate = audio_sample_rate
        self._set_audio_model()
        self.audio_channels = audio_channels
        self.audio_chunck_size = audio_chunck_size
        self._set_listener()
        self.audio_listening_time = audio_listening_time
        self.recognized_text = ""
        
        
        # ROS publisher
        # a publisher to publish the recognized text
        self.pub_stt = self.create_publisher(String, 'stt', 10)

        self.logger.info("Correctly initialized Speech to Text node")
        self.listen()

    #########################################################################################################
    # ----------------------------------------- # STT METHODS # ----------------------------------------- #
    #########################################################################################################

    def _set_listener(self):
        """
        Sets the listener used to capture the audio signal.

        Returns:
            True if the listener was set correctly.
        """
        self.logger.info("Setting listener...")
        
        self.listener = pyaudio.PyAudio()
        self.stream = self.listener.open(format=pyaudio.paInt16,
                                            channels=self.audio_channels,
                                            rate=self.audio_sample_rate,
                                            input=True,
                                            frames_per_buffer=self.audio_chunck_size)
        return True
        
    def _set_audio_model(self):
        """
        Sets the audio model used by the listener.
        
        Returns:
            True if the audio model was set correctly.
        """
        self.logger.info("Setting audio model...")
     
        self.audio_model = Model(self.audio_model_path)
        self.recognizer = KaldiRecognizer(self.audio_model, self.audio_sample_rate)
        return True
    
    def _stop_listener(self):
        """
        Stops the listener.

        Returns:
            True if the listener was stopped correctly.
        """
        self.logger.info("Stopping listener...")
        
        self.stream.stop_stream()
        self.stream.close()
        self.listener.terminate()
        return True
        
    def listen(self):
        """
        Listens to the audio signal and returns the recognized text.

        Returns:
            The recognized text.
        """
        
        self.logger.info("Listening...")
        self.recognized_text = ""
        self.stream.start_stream()
        
        while self.recognized_text == "":
            data = self.stream.read(self.audio_chunck_size, exception_on_overflow = False)
            if self.recognizer.AcceptWaveform(data):
                result= self.recognizer.Result()
                # self.logger.info("result: " + result)
                # text=result[14:-3]
                text=ast.literal_eval(result)['text']
                if text != "": 
                    self.recognized_text = (text + " ").replace("the ", "")
                    self.recognized_text = self.recognized_text.replace("huh", "")
                    if self.recognized_text != "" and self.recognized_text != " ": 
                        self.logger.info("Heard: " + self.recognized_text)
                        break

        self.pub_stt.publish(String(data=self.recognized_text))
        
        self.listen()
        
        return self.recognized_text 


    

def main():
    rclpy.init()

    client = STT()
    rclpy.spin(client)

    rclpy.shutdown()


if __name__ == '__main__':
    main()