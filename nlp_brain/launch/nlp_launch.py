from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='nlp_brain',
            # namespace='nlp_brain',
            executable='nlp',
            name='nlp',
        ),
        Node(
            package='nlp_brain',
            # namespace='nlp_brain',
            executable='stt',
            name='stt',
        ),

    ])
