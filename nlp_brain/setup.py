from setuptools import find_packages, setup
import os
from glob import glob

package_name = 'nlp_brain'
nlp_model='nlp_brain/nlp_model'

setup(
    name=package_name,
    version='0.0.0',
    # packages=find_packages(exclude=['test']),
    packages=[package_name, nlp_model],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # Include all launch files.
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*')))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='ludo',
    maintainer_email='ludo@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'nlp = nlp_brain.nlp:main',
            'stt= nlp_brain.stt:main',
        ],
    },
)
