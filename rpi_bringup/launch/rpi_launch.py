# Copyright (c) 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Main launch script to start the complete code on the real robot and in simulation"""

import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess, IncludeLaunchDescription
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PythonExpression
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def generate_launch_description():
 

    # Odrive launch files
    odrive = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(os.path.join(get_package_share_directory('odrive_demo_bringup'), 'launch'), 'odrive_multi_interface.launch.py')))

    # Pointcloud to laserscan launch files
    pointcloud_laserscan = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(os.path.join(get_package_share_directory('pointcloud_to_laserscan'), 'launch'), 'sample_pointcloud_to_laserscan_launch.py')),
        )
    
    # Livox launch files
    lidar_driver = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(os.path.join(get_package_share_directory('livox_ros_driver2'), 'launch_ROS2'), 'rviz_MID360_launch.py')),
        )

    # Create the launch description and populate
    ld = LaunchDescription()

    ld.add_action(odrive)
    ld.add_action(pointcloud_laserscan)
    ld.add_action(lidar_driver)

    return ld
