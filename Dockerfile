FROM althack/ros2:humble-cuda-full-2024-06-07

RUN adduser --quiet --disabled-password --gecos '' --uid ${UID:=1001} --uid ${GID:=1001} robocup
# Set the password for the robocup user: echo "$USER_NAME:$USER_PASS" | chpasswd
RUN echo "robocup:robocup" | chpasswd

RUN adduser robocup sudo
RUN usermod -aG staff robocup
RUN usermod -aG video robocup
# Allow non-root user to access the USB ports
RUN usermod -aG dialout robocup

COPY ros_entrypoint.sh /ros_entrypoint.sh

# Set the microphone and speaker port correctly
COPY nlp_brain/alsa.conf /usr/share/alsa/alsa.conf

RUN apt update \
    && apt install -y \
    ros-humble-rmw-cyclonedds-cpp \
    && rm -rf /var/lib/apt/lists/*

# For navigation related packages
RUN apt update && apt install -y --no-install-recommends \
    python3-pip python3-colcon-common-extensions \
    build-essential cmake nano git iproute2 \
    ros-humble-slam-toolbox \
    ros-humble-navigation2 \
    ros-humble-rviz2 \
    && rm -rf /var/lib/apt/lists/*

# For the nlp_brain package
RUN apt update && apt install -y --no-install-recommends \
    portaudio19-dev \
    unzip \
    alsa-base alsa-utils \
    && rm -rf /var/lib/apt/lists/*

# For the behavior_tree package
RUN apt install -y --no-install-recommends \
    ros-humble-behaviortree-cpp-v3


USER robocup

# For the nlp_brain package
RUN pip install -U pip wheel setuptools==70.0.0
RUN pip install spacy vosk pyaudio
RUN python3 -m spacy download en_core_web_sm
RUN pip install bitsandbytes
RUN pip install --upgrade accelerate transformers
RUN pip install pyttsx3


# For the face_recognition package
RUN pip install deepface==0.0.92
RUN pip install tf-keras
RUN pip install deprecated
RUN pip install portalocker

# For the human_pose_estimation package
RUN pip install numpy

WORKDIR /home/robocup

USER root

# For xarm_control package
# Note: here only installed the packge for the xArm python sdk. The packages need for the dynamixel (aka gripper control) is not installed.
RUN git clone https://github.com/xArm-Developer/xArm-Python-SDK.git
RUN cd xArm-Python-SDK && \
    python3 setup.py install

ENV RMW_IMPLEMENTATION='rmw_cyclonedds_cpp'

RUN usermod -aG audio robocup

USER robocup

WORKDIR /home/robocup/ros2_ws

RUN mkdir src

# Copy files
COPY --chown=robocup:robocup arm_interfaces src/arm_interfaces
COPY --chown=robocup:robocup xarm_interfaces src/xarm_interfaces
COPY --chown=robocup:robocup perception_interfaces src/perception_interfaces
COPY --chown=robocup:robocup nlp_brain_interface src/nlp_brain_interface
COPY --chown=robocup:robocup nav2_bringup src/nav2_bringup
COPY --chown=robocup:robocup rpi_bringup src/rpi_bringup
COPY --chown=robocup:robocup xarm_control src/xarm_control
COPY --chown=robocup:robocup face_recognition src/face_recognition
COPY --chown=robocup:robocup human_pose_estimation src/human_pose_estimation
COPY --chown=robocup:robocup gamepad src/gamepad
COPY --chown=robocup:robocup nlp_brain src/nlp_brain 
COPY --chown=robocup:robocup visualization src/visualization
COPY --chown=robocup:robocup zed-ros2-interfaces src/zed-ros2-interfaces

# Build the workspace
RUN . /opt/ros/humble/setup.sh
RUN colcon build

COPY --chown=robocup:robocup behavior_tree src/behavior_tree

# Build the workspace
RUN /bin/bash -c "source /home/robocup/ros2_ws/install/setup.bash && colcon build --packages-select behavior_tree"

WORKDIR /home/robocup
# Get the stt model for nlp_brain packages
RUN wget https://alphacephei.com/vosk/models/vosk-model-small-en-us-0.15.zip
RUN unzip /home/robocup/vosk-model-small-en-us-0.15.zip -d /home/robocup/ros2_ws/src/nlp_brain/nlp_brain
RUN rm -rf /home/robocup/vosk-model-small-en-us-0.15.zip

ENV NAV_LAUNCH_COMMAND='ros2 launch nav2_bringup bringup_launch.py'
ENV COMPONET_LAUNCH_COMMAND='ros2 launch behavior_tree component.launch.py'
ENV BT_LAUNCH_COMMAND='ros2 launch behavior_tree behavior_tree.launch.py'

# Create build and run aliases
RUN echo "source /ros_entrypoint.sh" >> /home/robocup/.bashrc && \
    echo "alias python=python3" >> /home/robocup/.bashrc && \
    echo "export NO_AT_BRIDGE=1" >> /home/robocup/.bashrc && \
    echo 'alias run_nav="/home/robocup/run_nav.sh"' >> /home/robocup/.bashrc && \
    echo "source /home/robocup/ros2_ws/install/setup.bash; $NAV_LAUNCH_COMMAND" >> /home/robocup/run_nav.sh && chmod +x /home/robocup/run_nav.sh && \
    echo 'alias run_component="/home/robocup/run_component.sh"' >> /home/robocup/.bashrc && \
    echo "source /home/robocup/ros2_ws/install/setup.bash; $COMPONET_LAUNCH_COMMAND" >> /home/robocup/run_component.sh && chmod +x /home/robocup/run_component.sh && \
    echo 'alias run_bt="/home/robocup/run_bt.sh"' >> /home/robocup/.bashrc && \
    echo "source /home/robocup/ros2_ws/install/setup.bash; $BT_LAUNCH_COMMAND" >> /home/robocup/run_bt.sh && chmod +x /home/robocup/run_bt.sh

USER robocup
WORKDIR /home/robocup/ros2_ws