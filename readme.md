# EPFL Robocup@Home
This is the main repository for the robocup@home. This is also the code used in the 2024 Eindhoven competition. The funcions are mainly built up for robot inspection and recpetionist task.

In this repository, it include the part for behavior tree, navigation, manipulation, nlp, face recognition, human pose recognition. 

The code related to zed camera and object detection is in "computer_vision" repository.

The code related to odriver control and livox is in "epfl_robocup_rpi" repository.

## Structure
### Behavior tree ([behavior_tree](/behavior_tree/))

### Navigation ([nav2_bringup](/nav2_bringup/) and [rpi_bringup](/rpi_bringup/))

### Computer Vision ([face_recognition](/face_recognition/), [human_pose_estimation](/human_pose_estimation/), [perception_interfaces](/perception_interfaces/), and [zed-ros2-interfaces](/zed-ros2-interfaces/))

### NLP ([nlp_brain](/nlp_brain/) and [nlp_brain_interfaces](/nlp_brain_interface/))

### Manipulation ([xarm_control](/xarm_control/) and [arm_interfaces](/arm_interfaces/))

### Others ([gamepad](/gamepad/))


## Prerequisites
- ROS2 Humble

RMW_IMPLEMENTATION=rmw_cyclonedds_cpp
```
# To set up run below command or put below command in ~/.bashrc
export RMW_IMPLEMENTATION=rmw_cyclonedds_cpp
```

## Getting Started
Here, we provided the docker file and a docker image to set up the environment, choose one fit your need. If you want to set up the environment directely on your local computer, you can follow the Dockerfile to install all neccesary libraries needed. If you are using robocup laptop, there has an environemnt already set up, please follow [How To Run Robot](/HowToRunRobot.md).

### Docker file

Clone the repo
```
git clone https://gitlab.epfl.ch/create-lab/robocup_at_home/epfl-robocup.git
git submodule init
git submodule update
```

Build docker container
```
# Go in to the repo
cd epfl-robocup

docker build -t robocup/robocup_main:test .
```

Run docker container
```
# Give access to GUI
xhost +

docker run --name YOUR_CONTAINER_NAME --privileged -it --network=host --runtime=nvidia -e DISPLAY -v /dev/bus/usb:/dev/bus/usb --device /dev/snd:/dev/snd robocup/robocup_main:test bash
```
* Instead of "--name YOUR_CONTAINER_NAME", you can use "--rm" which will delte the container after you exsit it. Check [here](https://docs.docker.com/reference/cli/docker/container/run/#rm) for more detail.

Run the ROS node

Note: In order to run the nlp node for the chatbot. The first time the code is runnung (ex. run_component), it will download the model "Meta-Llama-3-8B-Instruct". It need huggingface access token with access of the model, need to require on the [model's huggingface website](https://huggingface.co/meta-llama/Meta-Llama-3-8B-Instruct). Require in the website, and after it approved, put your hugging face access token in this script [chatbot.py](/nlp_brain/nlp_brain/nlp_model/chatbot.py). Only need for first time to download the model and it might take long time to download since the model is big.

```
# run the navigation related node. (in order to succesfully running this, need to running the rpi_bring on the rasberry pi first)
run_nav

# Open a new treminal and access to the container 
docker run --rm --privileged -it --network=host --runtime=nvidia -e DISPLAY -v /dev/bus/usb:/dev/bus/usb --device /dev/snd:/dev/snd robocup/robocup_main:test bash

# run the nlp, face reocgnition, xarm control ...
run_component

# Open a new treminal and access to the container 
docker run --rm --privileged -it --network=host --runtime=nvidia -e DISPLAY -v /dev/bus/usb:/dev/bus/usb --device /dev/snd:/dev/snd robocup/robocup_main:test bash

# run the behavior tree
run_bt
```

Access to docker container again 
```
docker start YOUR_CONTAINER_NAME

docker exec -it --user robocup YOUR_CONTAINER_NAME bash
```

### Dcoker Image

There is a docker image already set up and can be directely used by pulling form the docker hub. Note that, since it contain the model "Meta-Llama-3-8B-Instruct", so the total image size is 28.36 GB.

```
# Open a new treminal
# Give access to GUI
xhost +

# Create a docker container by pulling the image "changchuntzu/robocup_main:eindoven2024"
docker run --name=robocup_main_test --privileged -it --network=host --runtime=nvidia -e DISPLAY -v /dev/bus/usb:/dev/bus/usb --device /dev/snd:/dev/snd changchuntzu/robocup_main:eindoven2024 bash

# Exit the docker container first
exit

# Start the docker container again
docker start robocup_main_test

# Execute the docker container with user robocup
docker exec -it --user robocup robocup_main_test bash

# run the navigation related node. (in order to succesfully running this, need to running the rpi_bring on the rasberry pi first)
run_nav

# Open a new treminal and access to the container 
docker exec -it --user robocup robocup_main_test bash

# run the nlp, face reocgnition, xarm control ...
ros2 launch behavior_tree component.launch.py

# Open a new treminal and access to the container 
docker exec -it --user robocup robocup_main_test bash

# run the behavior tree
ros2 launch behavior_tree behavior_tree.launch.py
```



