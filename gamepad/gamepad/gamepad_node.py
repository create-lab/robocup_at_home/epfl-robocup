import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Joy
from .gamepad import Gamepad
from arm_interfaces.srv import ArmCommand
import time

class GamepadNode(Node):

    def __init__(self):
        super().__init__('gamepad_node')

        self.gamepad = Gamepad()

        # Publisher
        self.gamepad_publisher = self.create_publisher(Joy, '/gamepad', 10)

        # Subscriber
        self.gamepad_subscriber = self.create_timer(0.01, self.gamepad_callback) 

        # Service
        self.is_button_pressed = self.create_service(ArmCommand, 'is_button_pressed', self.is_button_pressed)

    def gamepad_callback(self):
        joy_data = Joy()

        joy_data.buttons = list(self.gamepad.button_data.values())
        joy_data.axes = list(self.gamepad.axis_data.values())

        self.gamepad_publisher.publish(joy_data)
        
    def is_button_pressed(self, request, response):
        cmd = request.command

        response.success = False
        timer = time.time()
        while time.time() - timer < 2:
            if self.gamepad.button_data[cmd] == 1:
                response.success = True
                break
        
        return response
            
def main(args=None):
    rclpy.init(args=args)

    gamepad_node = GamepadNode()

    rclpy.spin(gamepad_node)

    gamepad_node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()