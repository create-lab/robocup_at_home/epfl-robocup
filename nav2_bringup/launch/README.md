# launch

As stated in the main [README.md](../README.md), the first launch file that should be launched to start the complete code (drivers for the motors, SLAM, navigation stack) on the real robot, or the navigation stack and the simulated LIDAR on the simulated robot, is [robot_launch.py](robot_launch.py). This launch file then launches the other files recursively, as depicted in the following scheme:

![Launch scheme](launch_scheme.png)

In particular:
- [bringup_launch.py](bringup_launch.py) launches the SLAM launch file and the navigation launch file, and handles the use of composition;
- [rviz_launch.py](rviz_launch.py) allows to start the RViz visualization tool;
- [slam_launch.py](slam_launch.py) allows to start the SLAM toolbox;
- [navigation_launch.py](navigation_launch.py) allows to create all the nodes directly related to the navigation stack.

Furthermore, the launch file [odrive_multi_interface.launch.py](../../odrive_ros2_control/odrive_demo_bringup/launch/odrive_multi_interface.launch.py) allows to start the drivers od the motors, while the launch files [sample_pointcloud_to_laserscan_launch.py](../../pointcloud_to_laserscan/launch/sample_pointcloud_to_laserscan_launch.py) and [rviz_MID360_launch.py](../../livox_ros_driver2/launch_ROS2/rviz_MID360_launch.py) allows to start everything required to execute the LiDAR SLAM.

# NEW : launch files for the raspberry pi and the computer

- [rpi_launch.py](rpi_launch.py) launches the files for the raspberry pi
- [computer_launch.py](computer_launch.py) launches the files for the computer