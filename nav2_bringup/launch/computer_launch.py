# Copyright (c) 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Main launch script to start the complete code on the real robot and in simulation"""

import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess, IncludeLaunchDescription
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PythonExpression
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def generate_launch_description():
    # Get the launch directory
    bringup_dir = get_package_share_directory('nav2_bringup')
    launch_dir = os.path.join(bringup_dir, 'launch')

    # Create the launch configuration variables
    namespace = LaunchConfiguration('namespace')
    use_namespace = LaunchConfiguration('use_namespace')
    use_sim_time = LaunchConfiguration('use_sim_time')
    params_file = LaunchConfiguration('params_file')
    autostart = LaunchConfiguration('autostart')
    use_composition = LaunchConfiguration('use_composition')
    use_respawn = LaunchConfiguration('use_respawn')

    # # Launch configuration variables specific to simulation
    # rviz_config_file = LaunchConfiguration('rviz_config_file')
    # use_simulator = LaunchConfiguration('use_simulator')
    # use_robot_state_pub = LaunchConfiguration('use_robot_state_pub')
    # use_rviz = LaunchConfiguration('use_rviz')
    # world = LaunchConfiguration('world')
    # pose = {'x': LaunchConfiguration('x_pose', default='0.00'),
    #         'y': LaunchConfiguration('y_pose', default='0.00'),
    #         'z': LaunchConfiguration('z_pose', default='0.00'),
    #         'R': LaunchConfiguration('roll', default='0.00'),
    #         'P': LaunchConfiguration('pitch', default='0.00'),
    #         'Y': LaunchConfiguration('yaw', default='0.00')}
    # robot_name = LaunchConfiguration('robot_name')
    # robot_sdf = LaunchConfiguration('robot_sdf')

    # Map fully qualified names to relative ones so the node's namespace can be prepended.
    # In case of the transforms (tf), currently, there doesn't seem to be a better alternative
    # https://github.com/ros/geometry2/issues/32
    # https://github.com/ros/robot_state_publisher/pull/30
    # TODO(orduno) Substitute with `PushNodeRemapping`
    #              https://github.com/ros2/launch_ros/issues/56
    remappings = [('/tf', 'tf'),
                  ('/tf_static', 'tf_static')]

    # Declare the launch arguments
    declare_namespace_cmd = DeclareLaunchArgument(
        'namespace',
        default_value='',
        description='Top-level namespace')

    declare_use_namespace_cmd = DeclareLaunchArgument(
        'use_namespace',
        default_value='false',
        description='Whether to apply a namespace to the navigation stack')

    declare_use_sim_time_cmd = DeclareLaunchArgument(
        'use_sim_time',
        default_value='False',
        description='Use simulation (Gazebo) clock if true')
    
    # # Determine the params file to use based on whether we are in simulation or not
    # if use_sim_time:
    #     params_file_name = 'sim_nav2_params.yaml'
    # else:
    params_file_name = 'nav2_params.yaml'

    declare_params_file_cmd = DeclareLaunchArgument(
        'params_file',
        default_value=os.path.join(bringup_dir, 'params', params_file_name),
        description='Full path to the ROS2 parameters file to use for all launched nodes')
    
    # declare_use_simulator_cmd = DeclareLaunchArgument(
    #     'use_simulator',
    #     default_value='False',
    #     description='Whether to start the simulator')

    declare_autostart_cmd = DeclareLaunchArgument(
        'autostart', default_value='true',
        description='Automatically startup the nav2 stack')

    declare_use_composition_cmd = DeclareLaunchArgument(
        'use_composition', default_value='True',
        description='Whether to use composed bringup')

    declare_use_respawn_cmd = DeclareLaunchArgument(
        'use_respawn', default_value='False',
        description='Whether to respawn if a node crashes. Applied when composition is disabled.')

    declare_rviz_config_file_cmd = DeclareLaunchArgument(
        'rviz_config_file',
        default_value=os.path.join(
            bringup_dir, 'rviz', 'nav2_default_view.rviz'),
        description='Full path to the RVIZ config file to use')

    declare_use_robot_state_pub_cmd = DeclareLaunchArgument(
        'use_robot_state_pub',
        default_value='True',
        description='Whether to start the robot state publisher')

    declare_use_rviz_cmd = DeclareLaunchArgument(
        'use_rviz',
        default_value='True',
        description='Whether to start RVIZ')

    declare_robot_name_cmd = DeclareLaunchArgument(
        'robot_name',
        default_value='robot',
        description='name of the robot')
    

    # Definition of the name of the world SDF file
    # >> Modify the following line to load a different world SDF file <<
    world_file_name = 'world_only.model'
    # Definition of the name of the robot SDF file
    # >> Modify the following line to load a different robot SDF file <<
    robot_sdf_name = 'robot.model'
    # Definition of the name of the robot URDF file
    # >> Modify the following line to load a different robot URDF file <<
    robot_urdf_name = 'robot.urdf'
    
    declare_world_cmd = DeclareLaunchArgument(
        'world',
        default_value=os.path.join(bringup_dir, 'worlds', world_file_name),
        description='Full path to the world model file to load')
    
    declare_robot_sdf_cmd = DeclareLaunchArgument(
        'robot_sdf',
        default_value=os.path.join(bringup_dir, 'worlds', robot_sdf_name),
        description='Full path to the robot sdf file to spawn the robot in gazebo')
    
    # start_gazebo_server_cmd = ExecuteProcess(
    #     condition=IfCondition(use_simulator),
    #     cmd=['gzserver', '-s', 'libgazebo_ros_init.so',
    #          '-s', 'libgazebo_ros_factory.so', world],
    #     cwd=[launch_dir], output='screen')

    # start_gazebo_client_cmd = ExecuteProcess(
    #     condition=IfCondition(use_simulator),
    #     cmd=['gzclient'],
    #     cwd=[launch_dir], output='screen')

    urdf = os.path.join(bringup_dir, 'urdf', robot_urdf_name)
    with open(urdf, 'r') as infp:
        robot_description = infp.read()

    start_robot_state_publisher_cmd = Node(
        condition=IfCondition(use_robot_state_pub),
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        namespace=namespace,
        output='screen',
        parameters=[{'use_sim_time': use_sim_time,
                     'robot_description': robot_description}],
        remappings=remappings)
    
    # start_gazebo_spawner_cmd = Node(
    #     condition=IfCondition(use_simulator),
    #     package='gazebo_ros',
    #     executable='spawn_entity.py',
    #     output='screen',
    #     arguments=[
    #         '-entity', robot_name,
    #         '-file', robot_sdf,
    #         '-robot_namespace', namespace,
    #         '-x', pose['x'], '-y', pose['y'], '-z', pose['z'],
    #         '-R', pose['R'], '-P', pose['P'], '-Y', pose['Y']])

    rviz_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(launch_dir, 'rviz_launch.py')),
        condition=IfCondition(use_rviz),
        launch_arguments={'namespace': namespace,
                          'use_namespace': use_namespace,
                          'rviz_config': rviz_config_file}.items())

    bringup_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(launch_dir, 'bringup_launch.py')),
        launch_arguments={'namespace': namespace,
                          'use_namespace': use_namespace,
                          'use_sim_time': use_sim_time,
                          'params_file': params_file,
                          'autostart': autostart,
                          'use_composition': use_composition,
                          'use_respawn': use_respawn}.items())

    # Odrive launch files (launched only if not using simulator)
    # odrive = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(
    #         os.path.join(os.path.join(get_package_share_directory('odrive_demo_bringup'), 'launch'), 'odrive_multi_interface.launch.py')),
    #     condition=UnlessCondition(use_simulator))

    # nlp_node = Node(
    #     package="nlp_brain",
    #     executable="nlp",
    #     output="both",
    # )

    # brain_node = Node(
    #     package="nlp_brain",
    #     executable="brain",
    #     output="both",
    # )

    # # Pointcloud to laserscan launch files (launched only if not using simulator)
    # pointcloud_laserscan = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(
    #         os.path.join(os.path.join(get_package_share_directory('pointcloud_to_laserscan'), 'launch'), 'sample_pointcloud_to_laserscan_launch.py')),
    #     condition=UnlessCondition(use_simulator))
    
    # # Livox launch files (launched only if not using simulator)
    # lidar_driver = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource(
    #         os.path.join(os.path.join(get_package_share_directory('livox_ros_driver2'), 'launch_ROS2'), 'rviz_MID360_launch.py')),
    #     condition=UnlessCondition(use_simulator))

    # Create the launch description and populate
    ld = LaunchDescription()

    # Declare the launch options
    ld.add_action(declare_namespace_cmd)
    ld.add_action(declare_use_namespace_cmd)
    ld.add_action(declare_use_sim_time_cmd)
    ld.add_action(declare_params_file_cmd)
    ld.add_action(declare_autostart_cmd)
    ld.add_action(declare_use_composition_cmd)

    ld.add_action(declare_rviz_config_file_cmd)
    ld.add_action(declare_use_simulator_cmd)
    ld.add_action(declare_use_robot_state_pub_cmd)
    ld.add_action(declare_use_rviz_cmd)
    ld.add_action(declare_world_cmd)
    ld.add_action(declare_robot_name_cmd)
    ld.add_action(declare_robot_sdf_cmd)
    ld.add_action(declare_use_respawn_cmd)

    # Add any conditioned actions
    # ld.add_action(start_gazebo_server_cmd)
    # ld.add_action(start_gazebo_client_cmd)
    # ld.add_action(start_gazebo_spawner_cmd)
    # ld.add_action(odrive)
    # ld.add_action(pointcloud_laserscan)
    # ld.add_action(lidar_driver)

    # Add the actions to launch all of the navigation nodes
    # ld.add_action(start_robot_state_publisher_cmd)
    ld.add_action(rviz_cmd)
    ld.add_action(bringup_cmd)

    # ld.add_action(nlp_node)
    # ld.add_action(brain_node)

    return ld
