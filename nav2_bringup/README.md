# nav2_bringup (Navigation stack)

## Introduction

This is a robotic navigation system built with the [Navigation2 (Nav2)](https://github.com/ros-planning/navigation2) package. We have designed it to assist with autonomous navigation tasks, making use of the Robot Operating System (ROS) 2 ecosystem.

Our system is based on the 'humble' branch of the official [Navigation2 repository](https://github.com/ros-planning/navigation2/tree/humble).

## Features

The system supports real-world operation as well as a simulation mode. Navigation can be fully controlled using RViz, including setting goals and visualizing costmap and trajectories. Alternatively, a goal can be sent to a ROS2 action server (see below).

## Prerequisites

Ensure that you have the following installed on your system:

1. ROS 2 Humble: This project requires ROS 2 Humble. Follow [this guide](https://docs.ros.org/en/humble/Installation.html) to install ROS 2 Humble.
2. Gazebo: If the navigation stack is started in simulated mode, the Gazebo simulator is used. To install Gazebo, run: `sudo apt install gazebo ros-humble-gazebo-ros-pkgs`.

## Getting Started

In order to ensure reproducibility, and minimize the time required to have a functioning system, we created a Dockerfile to build and run the last stable version of the code. It is therefore recommended to use the Docker image to run the code, as detailed in the main [README.md](../README.md) of the repository.

### Setting up the workspace

1. Once in your workspace, source the ROS 2 installation:

```bash
source install/setup.bash
```

2. If the navigation stack is run in simulated mode, set the `GAZEBO_MODEL_PATH` environment variable:

```bash
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/opt/ros/humble/share/turtlebot3_gazebo/models:/home/<USER>/<WORKSPACE>/src
```
Substitute < USER > with your username, and < WORKSPACE > with the name of your workspace.

### Running the Robot in Real-World

1. Launch the robot:

```bash
ros2 launch nav2_bringup robot_launch.py
```

### Running the Robot in Simulation

1. Launch the robot in simulation:

```bash
ros2 launch nav2_bringup robot_launch.py use_sim_time:=True use_simulator:=True
```

## Sending a Navigation Command (goal)

As spacified above, it is possible to give commands to the robot by using RViz (both on the real robot and on the simulated robot). In any case, you can send a navigation command (goal) to the robot with the following command:

```bash
ros2 action send_goal /navigate_to_pose nav2_msgs/action/NavigateToPose "{pose: {header: {frame_id: 'map'}, pose: {position: {x: <X_POS>, y: <Y_POS>, z: 0.0}, orientation: {x: <X_OR>, y: <Y_OR>, z: <Z_OR>, w: <W_OR>}}}}"
```
Where <X_POS> should be substituted with the target position along X, and <Y_POS> should be substituted with the target position along Y. The target orientation is represented as a quaternion, and <X_OR>, <Y_OR>, <Z_OR>, <W_OR> are the components of the quaternion corresponding to the target orientation.

## Documentation
Apart from its stability, extensive support and ongoing development, one of the main advantages of Nav2 is its extensive documentation. The documentation about Nav2 can be accessed [at the following link](https://navigation.ros.org/). Some important documentation pages to consult are the following:
- [Getting started](https://navigation.ros.org/getting_started/index.html): This page contains important information on the installation process and on the first steps to be performed to run Nav2;
- [Development guides](https://navigation.ros.org/development_guides/index.html): This page contains guides for developing Nav2 (how to build from source, how to use dev containers...);
- [Navigation concepts](https://navigation.ros.org/concepts/index.html): This page introduces many important concepts which should be grasped before diving into Nav2's code;
- [General tutorials](https://navigation.ros.org/tutorials/index.html): This page introduces many tutorials related to Nav2;
- [Configuration guide](https://navigation.ros.org/configuration/index.html) and [Tuning guide](https://navigation.ros.org/tuning/index.html): These pages contain guides which assist users in tuning the navigation system. Indeed, Nav2 contains many tunable parameters, which can very often affect the performance of the system.

## Contact

Luca Zunino - luca.zunino@epfl.ch

Project Link: [https://gitlab.epfl.ch/create-lab/robocup_at_home/merge_dimitri_luca](https://gitlab.epfl.ch/create-lab/robocup_at_home/merge_dimitri_luca)

## Acknowledgements

- [ROS 2](https://docs.ros.org/en/humble/)
- [Navigation2 (Nav2)](https://navigation.ros.org/)