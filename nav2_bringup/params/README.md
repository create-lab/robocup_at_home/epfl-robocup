# params

This folder contains the yaml files listing all the parameters required for the Nav2 navigation stack. In particular, two files are present:
- [nav2_params.yaml](nav2_params.yaml), which is used when the code is run on the real robot;
- [sim_nav2_params.yaml](sim_nav2_params.yaml), which is used when the code is run in simulation.

While most of the parameters are the same on the real robot and on the simulated one, we kept two different files to allow for more fine-grained changes. For example, if the simulated environment is too small, it may be needed to slightly decrease the inflation radius around the obstacles in simulation.

The parameters have been tuned to reasonable values, but it may be necessary to further finetune them in the future if the robot's structure will change, or once the robot will be tested more extensively. Two documentations pages which can be useful in this regard are:
- [Nav2 Configuration Guide](https://navigation.ros.org/configuration/index.html), which contains a list and a short description of each parameter of the Nav2 navigation stack;
- [Nav2 Tuning Guide](https://navigation.ros.org/tuning/index.html), which contains instructions and suggestions to finetune the most relevant parameters.

Since each parameter is extensively detailed, we did not comment on all the parameters inside the yaml files. We just provided some pointers to the most critical parameters.