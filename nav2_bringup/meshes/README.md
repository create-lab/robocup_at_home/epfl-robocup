# meshes

This folder contains all the meshes which are needed to describe the robot and to simulate it within the Gazebo simulator. They are used within the [robot.urdf](../urdf/robot.urdf) file (URDF file describing the robot), and within the [robot.model](../worlds/robot.model) file (SDF file describing the robot model in Gazebo). In particular, the meshes which are contained in the folder are:
- [base_link.stl](base_link.stl), representing the base link;
- [reduction_1_1.stl](reduction_1_1.stl), representing the first reduction;
- [reduction_1_2.stl](reduction_1_2.stl), representing the second reduction;
- [reduction_1_3.stl](reduction_1_3.stl), representing the third reduction;
- [wheel_1_1.stl](wheel_1_1.stl), representing the first wheel;
- [wheel_1_2.stl](wheel_1_2.stl), representing the second wheel;
- [wheel_1_3.stl](wheel_1_3.stl), representing the third wheel;
- [motor_1_1.stl](motor_1_1.stl), representing the first motor;
- [motor_1_2.stl](motor_1_2.stl), representing the second motor;
- [motor_1_3.stl](motor_1_3.stl), representing the third motor.

If it is needed to change the representation of the robot, please modify the URDF accordingly, update the meshes in this folder and create again the SDF file of the robot ([robot.model](../worlds/robot.model)) as explained in the corresponding [folder](../worlds/).