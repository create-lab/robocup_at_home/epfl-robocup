# worlds

This folder contains the URDF file describing the robot in a suitable format for the Gazebo simulator ([robot.model](robot.model)) and two files which are describing worlds in which the robot can operate in Gazebo:
- [world_only_empty.model](world_only_empty.model), which allows to simulate the robot within a completely empty world;
- [world_only.model](world_only.model), which allows to simulate the robot within a world simulating a house with multiple rooms and obstacles (turtlebot3_house). This virtual environment is well-suited to test the navigation stack, as it contains multiple obstacles and narrow corridors.

Starting from the file [world_only.model](world_only.model), it is possible to change the environment within which the simulated robot operates by changing the uri of the model in this section:

```xml
<model name="turtlebot3_house">
    <static>1</static>
    <include>
        <uri>model://turtlebot3_house</uri>
    </include>
</model>
```

**IMPORTANT NOTE**: If the URDF file of the robot ([robot.urdf](../urdf/robot.urdf)) needs to be updated since the structure of the robot has been modified, it is necessary to update the URDF file ([robot.model](robot.model)) too. The procedure to obtain a new URDF file is the following:
```bash
gz sdf -p /path/to/urdf/robot.urdf > /path/to/sdf/robot.model
```
Where /path/to/urdf/ needs to be substituted with the path where the URDF file describing the robot is located, and /path/to/sdf/ with the path where the SDF file describing the robot is located. After obtaining the file, it is necessary to specify again the plugins used (sensor, controller, joint state publisher) within the SDF file.