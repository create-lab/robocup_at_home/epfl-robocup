#include "behavior_tree/nlp_bt.hpp"

namespace behavior_tree
{
    /**
     * @brief Construct a new NLP behavior tree Client:: NLP behavior tree Client object
     *
     * @param client_name The name of the node that is instantiated.
     */
    NLPBT::NLPBT(const std::string &client_name){
        node_ = rclcpp::Node::make_shared(client_name);

        say_client_ = node_->create_client<nlp_brain_interface::srv::Say>("say");
        ask_name_or_drink_action_ = rclcpp_action::create_client<nlp_brain_interface::action::GetName>(node_,"ask_name_or_drink");

        while (!say_client_->wait_for_service(1s) || !ask_name_or_drink_action_->wait_for_action_server(1s))
        {
            if (!rclcpp::ok()) {
                throw std::runtime_error("Interrupted while waiting for the service. Exiting.");
            }
            RCLCPP_INFO(node_->get_logger(), "%s service(s) not available, waiting again...", client_name.c_str());
        }
        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    bool NLPBT::robot_say(std::string text){
            auto request = std::make_shared<nlp_brain_interface::srv::Say::Request>();
            request->text = text;
            auto result_future = say_client_->async_send_request(request);
            // auto status = result_future.wait_for(20s);  //not spinning here!
            // if (status == std::future_status::ready)
            if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
            {
                RCLCPP_INFO(node_->get_logger(), "robot_say get response");
                return true;
            }
            else
            {
                RCLCPP_ERROR(node_->get_logger(), "Failed to call service say");
                return false;
            }
        }
    


    bool NLPBT::send_goal_ask_name_or_drink_action_(std::string question, std::string ask_for){
        //initiate send goal action
        send_goal_result=std::make_tuple(BT::NodeStatus::RUNNING, std::vector<std::string>{});

        auto goal = nlp_brain_interface::action::GetName::Goal();
        goal.question = question;
        goal.ask_for = ask_for;
        auto send_goal_options = rclcpp_action::Client<nlp_brain_interface::action::GetName>::SendGoalOptions();
        send_goal_options.goal_response_callback = std::bind(&NLPBT::goal_response_callback_ask_name_or_drink_action_, this, std::placeholders::_1);
        send_goal_options.feedback_callback = std::bind(&NLPBT::feedback_callback_ask_name_or_drink_action_, this, std::placeholders::_1, std::placeholders::_2);
        send_goal_options.result_callback = std::bind(&NLPBT::result_callback_ask_name_or_drink_action_, this, std::placeholders::_1);
        ask_name_or_drink_action_->async_send_goal(goal, send_goal_options);
        

        // Spin until the condition is met (e.g., goal completed or timeout)
        rclcpp::Rate loop_rate(10); // Adjust the rate as needed
        while (rclcpp::ok()) {
            if (send_goal_success != BT::NodeStatus::RUNNING){
                if (send_goal_success == BT::NodeStatus::FAILURE){
                    RCLCPP_INFO(node_->get_logger(), "BT: Goal was rejected by server");
                    return false;
                }
                else{ //send_goal_success[0] == BT::NodeStatus::SUCCESS
                    if (std::get<0>(send_goal_result) != BT::NodeStatus::RUNNING) {
                        RCLCPP_INFO(node_->get_logger(), "BT: Got result");
                        if (ask_for == "name"){
                            return check_person_name(send_goal_result);
                        }
                        else{
                            return check_drink_name(send_goal_result);
                        }
                    }
                }
            }
            // Spin once (non-blocking)
            rclcpp::spin_some(node_->get_node_base_interface());
            loop_rate.sleep();
        }
        return false;
    }

    void NLPBT::goal_response_callback_ask_name_or_drink_action_(const rclcpp_action::ClientGoalHandle<nlp_brain_interface::action::GetName>::SharedPtr & goal_handle){
        if (!goal_handle) {
            RCLCPP_ERROR(node_->get_logger(), "Goal was rejected by server");
            send_goal_success = BT::NodeStatus::FAILURE;
        }
        RCLCPP_INFO(node_->get_logger(), "Goal accepted by server, waiting for result");
        send_goal_success = BT::NodeStatus::SUCCESS;
    }


    void NLPBT::feedback_callback_ask_name_or_drink_action_(
        rclcpp_action::ClientGoalHandle<nlp_brain_interface::action::GetName>::SharedPtr,
        const std::shared_ptr<const nlp_brain_interface::action::GetName::Feedback> feedback)
    {
        feedback_ = feedback->feedback;
        // RCLCPP_INFO(node_->get_logger(), "Got Feedback: %s", feedback->feedback.c_str());
    }


    void NLPBT::result_callback_ask_name_or_drink_action_(const rclcpp_action::ClientGoalHandle<nlp_brain_interface::action::GetName>::WrappedResult & result){
        switch (result.code) {
            case rclcpp_action::ResultCode::SUCCEEDED:
                break;
            case rclcpp_action::ResultCode::ABORTED:
                RCLCPP_INFO(node_->get_logger(), "Goal was aborted");
                send_goal_result=std::make_tuple(BT::NodeStatus::FAILURE, std::vector<std::string>{});
                break;
            case rclcpp_action::ResultCode::CANCELED:
                RCLCPP_INFO(node_->get_logger(), "Goal was canceled");
                send_goal_result=std::make_tuple(BT::NodeStatus::FAILURE, std::vector<std::string>{});
                break;
            default:
                RCLCPP_ERROR(node_->get_logger(), "Unknown result code");
                send_goal_result=std::make_tuple(BT::NodeStatus::FAILURE, std::vector<std::string>{});
                break;
        }

        std::stringstream ss;
        auto names=result.result->names;
        for (auto name : result.result->names) {
        ss << name << " ";
        }
        RCLCPP_INFO(node_->get_logger(), "Ask_Name_Or_Drink Result received: %s", ss.str().c_str());
        send_goal_result=std::make_tuple(BT::NodeStatus::SUCCESS, names);
    }

    bool NLPBT::check_person_name(std::tuple<BT::NodeStatus, std::vector<std::string>> result){
        auto names=std::get<1>(result);
        if (names.size() == 0){
            RCLCPP_INFO(node_->get_logger(), "No name detected");
            return false;
        }
        else if (names.size() > 1){
            RCLCPP_INFO(node_->get_logger(), "More than one name detected, only the first one will be used");
            return false;
        }
        else{
            RCLCPP_INFO(node_->get_logger(), "Name detected: %s", names[0].c_str());
            return true;
        }
    }

    bool NLPBT::check_drink_name(std::tuple<BT::NodeStatus, std::vector<std::string>> result){
        auto names=std::get<1>(result);
        if (names.size() == 0){
            RCLCPP_INFO(node_->get_logger(), "No drink detected");
            return false;
        }
        else{
            return true;
        }
    }

    std::string NLPBT::get_person_name(bool check_result){
        auto names=std::get<1>(send_goal_result);
        if (check_result){
            return names[0];
        }
        else{
            return "";
        }
    }

    std::vector<std::string> NLPBT::get_drink_name(bool check_result){
        auto names=std::get<1>(send_goal_result);
        if (check_result){
            return names;
        }
        else{
            return std::vector<std::string>{}; //empty vector
        }
    }

    bool NLPBT::introduce_guest(std::string name, std::vector<std::string> drinks,bool is_first=false, std::vector<std::string> features={}){
        std::string drink_text;
        if (drinks.empty()) {
            drink_text = "";
        }
        else if (drinks.size() == 1) {
            drink_text=drinks[0];
        }
        else{
            for (size_t i = 0; i < drinks.size(); ++i) {
                drink_text += drinks[i];
                if (i == drinks.size() - 2) {
                    drink_text += " and ";
                } else if (i < drinks.size() - 2) {
                    drink_text += " ";
                }
            }
        }

        if (is_first){
            std::string gender=features[0];
            std::string age=features[1];
            std::string race=features[2];
            std::string emotion=features[3];

            auto text1 = "This is " + name + "  " + name + "'s favorite drinks are " + drink_text;
            auto text2 = "  " + name + " is " + gender + "  " + age + " years old and looks " + emotion + " ";
            robot_say(text1);

            return robot_say(text2);

        }
        else{
            auto text="This is "+name+"  "+name+"'s favorite drinks are "+drink_text;
            return robot_say(text);
        }
        
        
    }

    
    void NLPBT::register_nodes(BT::BehaviorTreeFactory &factory){
        factory.registerSimpleAction("RobotSay", [&](TreeNode &node)
            {   auto say_text = getInputValue<std::string>(node, "say_text");
                return bool_to_node_status(robot_say(say_text)); },
                {InputPort<std::string>("say_text")});
        
        factory.registerSimpleAction("AskName", [&](TreeNode &node)
            {   auto question=getInputValue<std::string>(node, "question_AskNameOrDrink");
                auto ask_for=getInputValue<std::string>(node, "ask_for_AskNameOrDrink");
                auto nodetsatus=send_goal_ask_name_or_drink_action_(question, ask_for);
                auto name=get_person_name(nodetsatus);
                node.setOutput("person_name", name);
                return bool_to_node_status(nodetsatus);},
            {InputPort<std::string>("question_AskNameOrDrink"), InputPort<std::string>("ask_for_AskNameOrDrink"),OutputPort<std::string>("person_name")});
        
        factory.registerSimpleAction("AskDrink", [&](TreeNode &node)
            {   auto question=getInputValue<std::string>(node, "question_AskNameOrDrink");
                auto ask_for=getInputValue<std::string>(node, "ask_for_AskNameOrDrink");
                auto nodetsatus=send_goal_ask_name_or_drink_action_(question, ask_for);
                auto name=get_drink_name(nodetsatus);
                node.setOutput("drinks", name);
                return bool_to_node_status(nodetsatus);},
            {InputPort<std::string>("question_AskNameOrDrink"), InputPort<std::string>("ask_for_AskNameOrDrink"),OutputPort<std::vector<std::string>>("drinks")});
        
        factory.registerSimpleAction("IntroGuest", [&](TreeNode &node)
            {   auto name = getInputValue<std::string>(node, "person_name");
                auto drinks = getInputValue<std::vector<std::string>>(node, "drinks");
                auto is_first=getInputValue<bool>(node, "is_first_guest");
                if (is_first){
                    auto features=getInputValue<std::vector<std::string>>(node, "person_features");
                    return bool_to_node_status(introduce_guest(name,drinks, is_first, features));
                }
                else{
                    return bool_to_node_status(introduce_guest(name,drinks));
                }},
                {InputPort<std::string>("person_name"), InputPort<std::vector<std::string>>("drinks"), InputPort<bool>("is_first_guest"), InputPort<std::vector<std::string>>("person_features")});
    }


    //an example for sending two requests at "same" time, creating parallel actions
    bool NLPBT::example_parallel_action(std::string text){
            auto request = std::make_shared<nlp_brain_interface::srv::Say::Request>();
            request->text = text;
           
            auto result_future = say_client_->async_send_request(request,std::bind(&NLPBT::say_robot_callback, this, std::placeholders::_1));

            auto send_goal_result=std::make_tuple(BT::NodeStatus::RUNNING, std::vector<std::string>{});

            auto goal = nlp_brain_interface::action::GetName::Goal();
            goal.question = "";
            goal.ask_for = "name";
            auto send_goal_options = rclcpp_action::Client<nlp_brain_interface::action::GetName>::SendGoalOptions();
            send_goal_options.goal_response_callback = std::bind(&NLPBT::goal_response_callback_ask_name_or_drink_action_, this, std::placeholders::_1);
            send_goal_options.feedback_callback = std::bind(&NLPBT::feedback_callback_ask_name_or_drink_action_, this, std::placeholders::_1, std::placeholders::_2);
            send_goal_options.result_callback = std::bind(&NLPBT::result_callback_ask_name_or_drink_action_, this, std::placeholders::_1);
            ask_name_or_drink_action_->async_send_goal(goal, send_goal_options);
            
            // Spin until the condition is met (e.g., goal completed or timeout)
            rclcpp::Rate loop_rate(10); // Adjust the rate as needed
            while (rclcpp::ok()) {
                if (test_result != BT::NodeStatus::RUNNING || send_goal_success != BT::NodeStatus::RUNNING){
                    if (test_result == BT::NodeStatus::SUCCESS){
                        RCLCPP_INFO(node_->get_logger(), "robot_say 1 success");
                    }
                    if (send_goal_success == BT::NodeStatus::SUCCESS){
                        if (std::get<0>(send_goal_result) != BT::NodeStatus::RUNNING) {
                            RCLCPP_INFO(node_->get_logger(), "BT: Got result");
                        }
                    }

                    if (test_result == BT::NodeStatus::SUCCESS && std::get<0>(send_goal_result) != BT::NodeStatus::RUNNING){
                       RCLCPP_INFO(node_->get_logger(), "Got two result");
                       return true;
                    }
                    
                }
                // Spin once (non-blocking)
                rclcpp::spin_some(node_->get_node_base_interface());
                loop_rate.sleep();
            }
            return false;
        }

    void NLPBT::say_robot_callback(const rclcpp::Client<nlp_brain_interface::srv::Say>::SharedFuture future){
        auto result = future.get();
        test_result = BT::NodeStatus::SUCCESS;
        RCLCPP_INFO(node_->get_logger(), "robot_say get response");
    }
}