#include "behavior_tree/object_detection_bt.hpp"

namespace behavior_tree
{
    /**
     * @brief Construct a new Object Detection Behavior Tree Client:: Object Detection Behavior Tree Client object
     *
     * @param client_name The name of the node that is instantiated.
     */
    ObjectDetectionBT::ObjectDetectionBT(const std::string &client_name)
    {
        node_ = rclcpp::Node::make_shared(client_name);
        get_object_info_client_ = node_->create_client<perception_interfaces::srv::GetObjectInfo>("get_object_info");
        get_point_pose_client_ = node_->create_client<perception_interfaces::srv::GetPointPose>("get_point_pose");
        get_object_pose_client_ = node_->create_client<perception_interfaces::srv::GetObjectPose>("get_object_pose");
        get_gripper_angle_client_ = node_->create_client<perception_interfaces::srv::GetGripperAngle>("get_gripper_angle");
        object_name_publisher_ = node_->create_publisher<perception_interfaces::msg::ObjectNames>("object_name", 10);
        empty_seats_publisher_ = node_->create_publisher<perception_interfaces::msg::ObjectInfos>("empty_seats", 10);
        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(node_->get_clock());
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

        
        while (!get_object_info_client_->wait_for_service(1s) || !get_point_pose_client_->wait_for_service(1s) )
        {
            if (!rclcpp::ok()) {
                throw std::runtime_error("Interrupted while waiting for the service and subscriber. Exiting.");
            }
            RCLCPP_INFO(node_->get_logger(), "%s service(s) not available, waiting again...", client_name.c_str());
        }

        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    std::tuple<bool, std::vector<perception_interfaces::msg::ObjectInfo>> ObjectDetectionBT::get_object_info(std::string object_name)
    {
        auto request = std::make_shared<perception_interfaces::srv::GetObjectInfo::Request>();
        request->object_name = object_name;
        auto result_future = get_object_info_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            bool is_object_found = result->is_object_found;
            std::vector<perception_interfaces::msg::ObjectInfo> object_infos= result->object_infos;
            if (is_object_found)
            {
                RCLCPP_INFO(node_->get_logger(), "Object found: %s", object_name.c_str());
                return {true, object_infos};
            }
            else
            {
                RCLCPP_INFO(node_->get_logger(), "Object not found: %s", object_name.c_str());
                return {false, std::vector<perception_interfaces::msg::ObjectInfo>()};
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_object_info");
            return {false, std::vector<perception_interfaces::msg::ObjectInfo>()};
        }
    }

    float ObjectDetectionBT::calculate_overlap(const std::vector<int16_t>& person_bbox, const std::vector<int16_t>& seat_bbox)
    {
        int x1 = std::max(person_bbox[0], seat_bbox[0]);
        int y1 = std::max(person_bbox[1], seat_bbox[1]);
        int x2 = std::min(person_bbox[2], seat_bbox[2]);
        int y2 = std::min(person_bbox[3], seat_bbox[3]);
        
        int intersection_area = std::max(0, x2 - x1) * std::max(0, y2 - y1);
        int seat_bbox_area = (seat_bbox[2] - seat_bbox[0]) * (seat_bbox[3] - seat_bbox[1]);
        RCLCPP_INFO(node_->get_logger(), "Intersection area: %d, Seat bbox area: %d", intersection_area, seat_bbox_area);   
        
        float overlap = static_cast<float>(intersection_area) / seat_bbox_area;
        RCLCPP_INFO(node_->get_logger(), "Overlap rate: %f", overlap);
        
        return overlap;
    }


    void ObjectDetectionBT::find_empty_chair(
        std::vector<perception_interfaces::msg::ObjectInfo> person_infos,
        std::vector<perception_interfaces::msg::ObjectInfo> chair_infos) 
    {
        RCLCPP_INFO(node_->get_logger(),"Start finding empty chair");
        float overlap_rate;
        int found_chair = 0;

        // Check chairs
        for (const auto& chair : chair_infos) {
            RCLCPP_INFO(node_->get_logger(), "Chair bbox: %d, %d, %d, %d", chair.object_box[0], chair.object_box[1], chair.object_box[2], chair.object_box[3]);
            bool occupied = false;
            for (const auto& person : person_infos) {
                RCLCPP_INFO(node_->get_logger(), "Person bbox: %d, %d, %d, %d", person.object_box[0], person.object_box[1], person.object_box[2], person.object_box[3]);
                overlap_rate=calculate_overlap(person.object_box, chair.object_box);
                RCLCPP_INFO(node_->get_logger(), "Overlap rate: %f", overlap_rate);
                if (overlap_rate > 0.7) {
                    occupied = true;
                    break;
                }
            }
            if (!occupied) {
                empty_seats.push_back(chair);
                found_chair++;
            }
        }
        RCLCPP_INFO(node_->get_logger(), "Found %d empty chairs", found_chair);
    }
        

    void ObjectDetectionBT::find_empty_couch(
        std::vector<perception_interfaces::msg::ObjectInfo> person_infos,
        std::vector<perception_interfaces::msg::ObjectInfo> couch_infos) 
    {
        RCLCPP_INFO(node_->get_logger(),"Start finding empty couch");
        float overlap_rate;
        int found_couch = 0;

        // Check couches
        for (auto& couch : couch_infos) {
            bool occupied = false;
            for (const auto& person : person_infos) {
                overlap_rate=calculate_overlap(person.object_box, couch.object_box);
                if (overlap_rate > 0.9) {
                    occupied = true;
                    break;
                }
                else if (overlap_rate > 0.5) {
                    if (person.object_box[0] < couch.object_box[0] && person.object_box[2] > couch.object_box[0]) {
                        couch.object_box[0]=person.object_box[2];
                        couch.object_name="couch_cut";
                    }
                    else if (person.object_box[2] > couch.object_box[2] && person.object_box[0] < couch.object_box[2]) {
                        couch.object_box[2]=person.object_box[0];
                        couch.object_name="couch_cut";
                    }
                    else if (person.object_box[0] > couch.object_box[0] && person.object_box[2] < couch.object_box[2]){
                        if (person.object_box[0] - couch.object_box[0] < couch.object_box[2] - person.object_box[2]){
                            couch.object_box[0]=person.object_box[2];
                            couch.object_name="couch_cut";
                        }
                        else{
                            couch.object_box[2]=person.object_box[0];
                            couch.object_name="couch_cut";
                        }
                    }
                }   
            }
            if (!occupied) {
                empty_seats.push_back(couch);
                found_couch++;
            }
        }
        RCLCPP_INFO(node_->get_logger(), "Found %d empty couches", found_couch);
    }

    std::tuple<bool,geometry_msgs::msg::Pose> ObjectDetectionBT::find_empty_seat(std::string target_frame){
        RCLCPP_INFO(node_->get_logger(),"Get final empty seat");
        bool is_success=false;
        geometry_msgs::msg::Pose pose=geometry_msgs::msg::Pose();

        //if no empty seat found, return false
        if (empty_seats.empty()) {
            RCLCPP_INFO(node_->get_logger(), "No empty seat found");
            return {false, geometry_msgs::msg::Pose()};
        }

        // Print the empty seats
        for (const auto& seat : empty_seats) {
            RCLCPP_INFO(node_->get_logger(), "Empty seat found: %s, %d, %d, %d, %d", seat.object_name.c_str(), seat.object_box[0], seat.object_box[1], seat.object_box[2], seat.object_box[3]);
        }
       
        perception_interfaces::msg::ObjectInfo biggest_empty_seat;
        //get the biigest bbox among all empty seat
        if (empty_seats.size() == 1) {
            biggest_empty_seat=empty_seats[0];
        }
        else{
            int max_area = 0;
            for (const auto& seat : empty_seats) {
                int area = (seat.object_box[2] - seat.object_box[0]) * (seat.object_box[3] - seat.object_box[1]);
                if (area > max_area) {
                    max_area = area;
                    biggest_empty_seat = seat;
                }
            }
            RCLCPP_INFO(node_->get_logger(), "Empty seat for guest: %s, %d, %d, %d, %d", biggest_empty_seat.object_name.c_str(), biggest_empty_seat.object_box[0], biggest_empty_seat.object_box[1], biggest_empty_seat.object_box[2], biggest_empty_seat.object_box[3]);
        }

        //publish all empty seats, and the last one is the biggest empty seat/final empty seat point to guest
        auto msg = perception_interfaces::msg::ObjectInfos();
        auto publish_empty_seats=empty_seats;
        publish_empty_seats.push_back(biggest_empty_seat);
        msg.object_infos = publish_empty_seats;
        empty_seats_publisher_->publish(msg);

        if (biggest_empty_seat.object_name=="couch_cut"){
            RCLCPP_INFO(node_->get_logger(), "Couch is cut, ask for get the point pose service..");
            //get the middle point of the biggest empty seat
            double middle_x = (biggest_empty_seat.object_box[0] + biggest_empty_seat.object_box[2]) / 2;
            double middle_y = (biggest_empty_seat.object_box[1] + biggest_empty_seat.object_box[3]) / 2;
            std::tie(is_success, pose)=get_point_pose({middle_x, middle_y}, "couch", target_frame);
        }

        else{
            auto object_transform=biggest_empty_seat.object_transform;
            std::tie(is_success, pose)=transform(object_transform, target_frame);
        }

        if (!is_success){
                return {false, geometry_msgs::msg::Pose()};
            }
        RCLCPP_INFO(node_->get_logger(), "Final empty seat pose for guest in %s frame: x: %f, y: %f, z: %f",target_frame.c_str(), pose.position.x, pose.position.y, pose.position.z);
        return {is_success, pose};
    }


    /**
     * @brief Get the point pose in the target frame. Calling s ervice: given a point, give back the TransformStamped
     *
     * @param point The point to find.
     * @param object_name The name of the object to find.
     * @return std::tuple<bool, geometry_msgs::msg::Pose> A tuple containing a boolean that indicates if the point was found and
     * the pose of the point in the target frame.
     */

    std::tuple<bool, geometry_msgs::msg::Pose> ObjectDetectionBT::get_point_pose(std::vector<double> point,std::string object_name, std::string target_frame)
    {
        auto request = std::make_shared<perception_interfaces::srv::GetPointPose::Request>();
        request->object_name = object_name;
        geometry_msgs::msg::Point point_msg;
        point_msg.x = point[0];
        point_msg.y = point[1];
        request->point = point_msg;

        auto result_future = get_point_pose_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();

            if (!result->is_success)
            {
                RCLCPP_INFO(node_->get_logger(), "Failed to get point pose");
                return {false, geometry_msgs::msg::Pose()};
            }
            if (!target_frame.empty())
            {
                geometry_msgs::msg::TransformStamped object_transform = result->object_transform;
                auto[is_success, pose]=transform(object_transform, target_frame);
                if (!is_success){
                    return {false, geometry_msgs::msg::Pose()};
                }
                
                RCLCPP_INFO(node_->get_logger(), "Point pose in %s frame: x: %f, y: %f, z: %f",target_frame.c_str(), pose.position.x, pose.position.y, pose.position.z);
                return {true, pose};
            }
            else
            {   
                geometry_msgs::msg::TransformStamped object_transform = result->object_transform;
                geometry_msgs::msg::Pose pose = geometry_msgs::msg::Pose();
                pose.position.x = object_transform.transform.translation.x;
                pose.position.y = object_transform.transform.translation.y;
                pose.position.z = object_transform.transform.translation.z;
                pose.orientation.x = object_transform.transform.rotation.x;
                pose.orientation.y = object_transform.transform.rotation.y;
                pose.orientation.z = object_transform.transform.rotation.z;
                pose.orientation.w = object_transform.transform.rotation.w;
                RCLCPP_INFO(node_->get_logger(), "Point pose in camera frame: x: %f, y: %f, z: %f", pose.position.x, pose.position.y, pose.position.z);
                return {true, pose};
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_object_pose");
            return {false, geometry_msgs::msg::Pose()};
        }
    }


    
    /**
     * @brief Get the object pose in the target frame.
     *
     * @param object_name The name of the object to find.
     * @return std::tuple<bool, geometry_msgs::msg::Pose> A tuple containing a boolean that indicates if the object was found and
     * the pose of the object in the tcp frame.
     */
    std::tuple<bool, geometry_msgs::msg::Pose> ObjectDetectionBT::get_object_pose(std::string object_name, std::string target_frame)
    {
        auto request = std::make_shared<perception_interfaces::srv::GetObjectPose::Request>();
        request->object_name = object_name;
        auto result_future = get_object_pose_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();

            if (!result->is_object_found)
            {
                RCLCPP_INFO(node_->get_logger(), "%s not found", object_name.c_str());
                return {false, geometry_msgs::msg::Pose()};
            }
            if (!target_frame.empty())
            {
                geometry_msgs::msg::TransformStamped object_transform = result->object_transform;
                auto[is_success, pose]=transform(object_transform, target_frame);
                if (!is_success){
                    return {false, geometry_msgs::msg::Pose()};
                }
                RCLCPP_INFO(node_->get_logger(), "%s object pose in %s frame: x: %f, y: %f, z: %f",object_name.c_str(), target_frame.c_str(), pose.position.x, pose.position.y, pose.position.z);
                return {true, pose};
            }
            else
            {   
                RCLCPP_INFO(node_->get_logger(), "%s found. Pose is empty since target frame is not given.", object_name.c_str());
                return {true, geometry_msgs::msg::Pose()};
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_object_pose");
            return {false, geometry_msgs::msg::Pose()};
        }
    }

    /**
     * @brief Get the angle in degrees by how much the gripper should rotate to pick the object.
     *
     * @param object_name The name of the object to find.
     * @return std::tuple<bool, double> A tuple containing a boolean that indicates if the object was found and
     * the angle in degrees by how much the gripper should rotate to pick the object.
     */
    std::tuple<bool, double> ObjectDetectionBT::get_gripper_angle(std::string object_name)
    {
        auto request = std::make_shared<perception_interfaces::srv::GetGripperAngle::Request>();
        request->object_name = object_name;
        auto result_future = get_gripper_angle_client_->async_send_request(request);
        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
            rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            RCLCPP_INFO(node_->get_logger(), "%s gripper angle: %f",object_name.c_str(), result->gripper_angle);
            return {result->is_object_found, result->gripper_angle};
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_gripper_angle");
            return {false, 0.0};
        }
    }

    /**
    * @brief Publishes the name of the object to detect to the object_name topic.
    *
    * @param object_name The name of the object to detect.
    */
    void ObjectDetectionBT::publish_object_name(std::vector<std::string> object_names)
    {
        auto msg = perception_interfaces::msg::ObjectNames();
        msg.object_names = object_names;
        object_name_publisher_->publish(msg);

        std::string object_names_str = "";
        for (auto name : object_names)
        {
            object_names_str += name + " ";
        }
        RCLCPP_INFO(node_->get_logger(), "Object names published: %s", object_names_str.c_str());
        
    }

    void ObjectDetectionBT::clean_empty_seats_visualization(){
        RCLCPP_INFO(node_->get_logger(), "Clean empty seats visualization");
        auto msg = perception_interfaces::msg::ObjectInfos();
        msg.object_infos = {};
        empty_seats_publisher_->publish(msg);
    }

    std::tuple<bool, geometry_msgs::msg::Pose> ObjectDetectionBT::transform(geometry_msgs::msg::TransformStamped object_transform, std::string target_frame)
    {
        geometry_msgs::msg::Pose pose = geometry_msgs::msg::Pose();

        // transform the object pose from the frame of object_transform to target frame
        try
        {
            auto t = tf_buffer_->transform(object_transform, target_frame, tf2::durationFromSec(1.0));

            pose.position.x = t.transform.translation.x;
            pose.position.y = t.transform.translation.y;
            pose.position.z = t.transform.translation.z;
            pose.orientation.x = t.transform.rotation.x;
            pose.orientation.y = t.transform.rotation.y;
            pose.orientation.z = t.transform.rotation.z;
            pose.orientation.w = t.transform.rotation.w;
        }
        catch (const tf2::TransformException &ex)
        {
            RCLCPP_INFO(node_->get_logger(), "Could not transform object from %s into %s frame: %s",object_transform.child_frame_id.c_str(),target_frame.c_str(), ex.what());
            return {false, geometry_msgs::msg::Pose()};
        }
        return {true, pose};
    }

    void ObjectDetectionBT::register_nodes(BT::BehaviorTreeFactory &factory)
    {
        factory.registerSimpleAction("GetObjectPose", [&](TreeNode &node)
                                     {  auto object_name = getInputValue<std::string>(node, "object_name");
                                        auto target_frame = getInputValue<std::string>(node, "target_frame");
                                        auto [is_object_found, object_pose] = get_object_pose(object_name, target_frame);
                                        std::vector<double> pose_vector= {object_pose.position.x, object_pose.position.y, object_pose.position.z, object_pose.orientation.x, object_pose.orientation.y, object_pose.orientation.z, object_pose.orientation.w};
                                        node.setOutput("object_pose", pose_vector);
                                        return bool_to_node_status(is_object_found); },
                                     {InputPort<std::string>("object_name"), InputPort<std::string>("target_frame"), OutputPort<std::vector<double>>("object_pose")});

       
        factory.registerSimpleAction("GetGripperAngle", [&](TreeNode &node)
                                     {  auto object_name = getInputValue<std::string>(node, "object_name");
                                        auto [is_object_found, gripper_angle] = get_gripper_angle(object_name);
                                        node.setOutput("gripper_angle", gripper_angle);
                                        return bool_to_node_status(is_object_found); },
                                     {InputPort<std::string>("object_name"), OutputPort<double>("gripper_angle")});


        factory.registerSimpleCondition("IsObjectFound", [&](TreeNode &node)
                                        {  auto object_name = getInputValue<std::string>(node, "object_name");
                                        auto [is_object_found, _] = get_object_pose(object_name, "");
                                        return bool_to_node_status(is_object_found); },
                                        {InputPort<std::string>("object_name")});
        
        factory.registerSimpleAction("PublishObjectName", [&](TreeNode &node)
                                         {  auto object_names = getInputValue<std::vector<std::string>>(node, "object_name");
                                            publish_object_name(object_names);
                                            sleep(2);
                                            return BT::NodeStatus::SUCCESS; },
                                         {InputPort<std::string>("object_name")});
        
        factory.registerSimpleAction("FindObject", [&](TreeNode &node)
                                         {  auto object_name = getInputValue<std::string>(node, "object_name");
                                            auto [is_object_found, object_infos] = get_object_info(object_name);
                                            if (is_object_found) {
                                                RCLCPP_INFO(node_->get_logger(), "Save object info in the dict");
                                                object_infos_dict_.insert({object_name, object_infos});
                                            }
                                            return bool_to_node_status(is_object_found);},
                                         {InputPort<std::string>("object_name")});

        factory.registerSimpleAction("FindEmptySeat", [&](TreeNode &node)
                                         {  
                                            empty_seats.clear();
                                            auto is_chair_found = object_infos_dict_.find("chair") != object_infos_dict_.end();
                                            auto is_couch_found = object_infos_dict_.find("couch") != object_infos_dict_.end();
                                            if (!is_chair_found && !is_couch_found){
                                                RCLCPP_INFO(node_->get_logger(), "Both chair and couch are not found");
                                                return BT::NodeStatus::FAILURE;
                                            }
                                            if (is_chair_found){
                                                find_empty_chair(object_infos_dict_["person"], object_infos_dict_["chair"]);
                                            }
                                            if (is_couch_found){
                                                find_empty_couch(object_infos_dict_["person"], object_infos_dict_["couch"]);
                                            }
                                            auto target_frame = getInputValue<std::string>(node, "target_frame");
                                            auto [is_empty_seat_found, seat_pose] = find_empty_seat(target_frame);
                                            std::vector<double> pose_vector= {seat_pose.position.x, seat_pose.position.y, seat_pose.position.z, seat_pose.orientation.x, seat_pose.orientation.y, seat_pose.orientation.z, seat_pose.orientation.w};
                                            node.setOutput("empty_seat_pose", pose_vector);
                                            return bool_to_node_status(is_empty_seat_found); },
                                            {InputPort<std::string>("target_frame"), OutputPort<std::vector<double>>("empty_seat_pose")});
        
        factory.registerSimpleCondition("IsPersonChairCouchFound", [&](TreeNode &)
                                        {   
                                            if (object_infos_dict_.find("person") == object_infos_dict_.end()){
                                                object_infos_dict_.insert({"person", {}});
                                            }
                                            auto is_chair_found = object_infos_dict_.find("chair") != object_infos_dict_.end();
                                            auto is_couch_found = object_infos_dict_.find("couch") != object_infos_dict_.end();
                                            if (!is_chair_found && !is_couch_found){
                                                RCLCPP_INFO(node_->get_logger(), "Both chair and couch are not found");
                                                return BT::NodeStatus::FAILURE;
                                            }
                                            return BT::NodeStatus::SUCCESS;
                                        });

        
        factory.registerSimpleAction("CleanObjectDict", [&](TreeNode &node)
                                         {  object_infos_dict_.clear();
                                            return BT::NodeStatus::SUCCESS; },
                                         {});

        factory.registerSimpleAction("CleanEmptySeatVisual", [&](TreeNode &node)
                                         {
                                            clean_empty_seats_visualization();
                                            return BT::NodeStatus::SUCCESS;
                                            },
                                         {});
        
        factory.registerSimpleCondition("IsDoorOpen", [&](TreeNode &node)
                                         {
                                            auto [is_success, pose]=get_point_pose({180,320}, "","");
                                            RCLCPP_INFO(node_->get_logger(), "is success: %d", is_success);
                                            if (!is_success){
                                                return BT::NodeStatus::FAILURE;
                                            }
                                            RCLCPP_INFO(node_->get_logger(), "Door pose in camera frame: x: %f, y: %f, z: %f", pose.position.x, pose.position.y, pose.position.z);
                                            //if the midle point depth is bigger than 2m, the door is open
                                            if (pose.position.x > 0.7){
                                                RCLCPP_INFO(node_->get_logger(), "Door is open");
                                                return BT::NodeStatus::SUCCESS;
                                            }
                                            RCLCPP_INFO(node_->get_logger(), "Door is closed");
                                            return BT::NodeStatus::FAILURE;
                                         });

    }
}

