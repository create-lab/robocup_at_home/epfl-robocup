
#include "behavior_tree/nlp_bt.hpp"
#include "behavior_tree/face_recognition_bt.hpp"
#include "behavior_tree/object_detection_bt.hpp"
#include "behavior_tree/human_pose_estimation_bt.hpp"
#include "behavior_tree/navigation_bt.hpp"
#include "behavior_tree/arm_bt.hpp"
#include "behavior_tree/gamepad_bt.hpp"
#include <ament_index_cpp/get_package_share_directory.hpp>
#include <csignal>



namespace behavior_tree{
    class MainBT : public rclcpp::Node
    {
        private:
            std::vector<double> arm_initial_joint={0.0750, -1.3125, 0.0750, 0.0105, -0.3089, -0.0768};
            behavior_tree::ArmControl arm_;
            behavior_tree::ObjectDetectionBT object_detection_;
            behavior_tree::NavBT navigation_;
            behavior_tree::NLPBT nlpbt_;
            behavior_tree::FaceRecognition face_recognition_;
            // behavior_tree::Gamepad gamepad_;
            // behavior_tree::HumanPoseEstimation human_pose_estimation_;

        public:
            MainBT(const rclcpp::NodeOptions &node_options) : 
                Node("main_bt", node_options),
                arm_("arm_behavior_tree"),
                object_detection_("object_detection_behavior_tree"),
                navigation_("navigation_behavior_tree"),
                nlpbt_("nlp_behavior_tree"),
                face_recognition_("face_recognition_behavior_tree")
                // gamepad_("gamepad_behavior_tree")
                // human_pose_estimation_("human_pose_estimation_tree"),
            {
                sleep(2);
            }

            void main_control()
            {
                RCLCPP_INFO(this->get_logger(), "main_control start");
                BT::BehaviorTreeFactory factory;
                register_nodes(factory);
                arm_.register_nodes(factory);
                navigation_.register_nodes(factory);
                object_detection_.register_nodes(factory);
                nlpbt_.register_nodes(factory);
                face_recognition_.register_nodes(factory);
                // gamepad_.register_nodes(factory);
                // human_pose_estimation_.register_nodes(factory);
                
                
                RCLCPP_INFO(this->get_logger(), "load behavior tree");
                auto package_path = ament_index_cpp::get_package_share_directory("behavior_tree");
                auto tree_file_path =package_path + "/bt_trees/main_tree.xml";
                auto tree = factory.createTreeFromFile(tree_file_path);

                RCLCPP_INFO(this->get_logger(), "start behavior tree");


                tree.tickRootWhileRunning();
            }

            bool press_enter() {
                std::cout << "Press 'Enter' to start/continue..." << std::endl;
                char c = std::cin.get();
                if (c == '\n') {
                    return true;
                }
                return false;
            }

            void register_nodes(BT::BehaviorTreeFactory &factory)
            {
                factory.registerSimpleAction("CombinedSentence", [&](TreeNode &node)
                {   auto part1=getInputValue<std::string>(node, "part1");
                    auto part2=getInputValue<std::string>(node, "part2");
                    auto part3=getInputValue<std::string>(node, "part3");
                    auto part4=getInputValue<std::string>(node, "part4");
                    auto part5=getInputValue<std::string>(node, "part5");
                    auto combinedSentence=part1+part2+part3+part4+part5;
                    node.setOutput("combinedSentence", combinedSentence);
                    return BT::NodeStatus::SUCCESS;},
                {InputPort<std::string>("part1"), InputPort<std::string>("part2"), InputPort<std::string>("part3"), InputPort<std::string>("part4"), InputPort<std::string>("part5"),OutputPort<std::string>("combinedSentence")});

                factory.registerSimpleCondition("IsEnterPress", [&](TreeNode &node)
                                         {
                                            return bool_to_node_status(press_enter());
                                         });
            }
    };
}

void signal_handler(int signum) {
    RCLCPP_ERROR(rclcpp::get_logger("main_bt"), "Interrupt signal (%d) received. Shutting down.", signum);
    rclcpp::shutdown();
}


int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    rclcpp::NodeOptions node_options;
    node_options.automatically_declare_parameters_from_overrides(true);

    signal(SIGINT, signal_handler);


    std::shared_ptr<behavior_tree::MainBT> main_node = std::make_shared<behavior_tree::MainBT>(node_options);
    
    rclcpp::executors::SingleThreadedExecutor executor;
    executor.add_node(main_node);
    std::thread([&executor]()
                { executor.spin(); })
        .detach();


    RCLCPP_INFO(main_node->get_logger(), "main_bt start");
    main_node->main_control();
    RCLCPP_INFO(main_node->get_logger(), "main_bt over");
    rclcpp::shutdown();
    return 0;
    

}