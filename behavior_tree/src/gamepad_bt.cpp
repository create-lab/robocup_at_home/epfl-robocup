#include "behavior_tree/gamepad_bt.hpp"

// TODO: The code do not use in competition so it is not test either.

namespace behavior_tree
{
    /**
     * @brief Construct a new face recognition behavior tree Client:: FaceRecognition behavior tree Client object
     *
     * @param client_name The name of the node that is instantiated.
     */
    Gamepad::Gamepad(const std::string &client_name){
        node_ = rclcpp::Node::make_shared(client_name);

        is_button_pressed_client_ = node_->create_client<arm_interfaces::srv::ArmCommand>("is_button_pressed");
        
        while (!is_button_pressed_client_->wait_for_service(1s))
        {
            if (!rclcpp::ok()) {
                throw std::runtime_error("Interrupted while waiting for the service. Exiting.");
            }
            RCLCPP_INFO(node_->get_logger(), "%s service(s) not available, waiting again...", client_name.c_str());
        }
        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    // command: "up", "down", "left", "right"
    bool Gamepad::is_button_press(std::string command){
        auto request = std::make_shared<arm_interfaces::srv::ArmCommand::Request>();
        request->command = command;
        auto result_future = is_button_pressed_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Button is pressed");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Button is not pressed");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service is_button_pressed");
            return false;
        }
    }

    void Gamepad::register_nodes(BT::BehaviorTreeFactory &factory){
        
        factory.registerSimpleCondition("IsButtonPressed", [&](TreeNode &)
            {   
               return bool_to_node_status(is_button_press("up"));
            });
    }
}