#include "behavior_tree/arm_bt.hpp"

namespace behavior_tree
{
    /**
     * @brief Construct a new xarm control behavior tree Client:: Xarm behavior tree 
     *
     * @param client_name The name of the node that is instantiated.
     */
    ArmControl::ArmControl(const std::string &client_name){
        node_ = rclcpp::Node::make_shared(client_name);

        move_joint_client_ = node_->create_client<arm_interfaces::srv::ArmCommand>("move_joint");
        set_target_from_cam_client_ = node_->create_client<arm_interfaces::srv::ArmCommand>("set_target");
        move_pose_client_ = node_->create_client<arm_interfaces::srv::ArmCommand>("move_pose");
        move_gripper_client_ = node_->create_client<arm_interfaces::srv::ArmCommand>("move_gripper");
        
        while (!move_joint_client_->wait_for_service(1s))
        // || !set_target_from_cam_client_->wait_for_service(1s)
        // || !move_pose_client_->wait_for_service(1s) || !move_gripper_client_->wait_for_service(1s))
        {
            if (!rclcpp::ok()) {
                throw std::runtime_error("Interrupted while waiting for the service. Exiting.");
            }
            RCLCPP_INFO(node_->get_logger(), "%s service(s) not available, waiting again...", client_name.c_str());
        }
        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    bool ArmControl::move_joint(std::vector<double> angles, std::string command){
        // commad: if "relative": given relative angles, otherwise given absolute angles
        // std::vector<double> arm_initial_joint={-1.7,-64,-13,-1.1,-15.9,0.0}; //degree
        auto request = std::make_shared<arm_interfaces::srv::ArmCommand::Request>();
        //request->data: angles in degree
        request->data = angles;
        request->command = command;
        RCLCPP_INFO(node_->get_logger(), "move_joint: %f %f %f %f %f %f", request->data[0], request->data[1], request->data[2], request->data[3], request->data[4], request->data[5]);
        RCLCPP_INFO(node_->get_logger(),"command: %s",command.c_str());
        auto result_future = move_joint_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Joint move success");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Joint move failed");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service move_joint");
            return false;
        }
    }

    bool ArmControl::set_target(std::vector<double> pose, std::string command){
        auto request = std::make_shared<arm_interfaces::srv::ArmCommand::Request>();
        request->data = pose;
        request->command = command;
        auto result_future = set_target_from_cam_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Set target success");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Set target failed");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service set_target");
            return false;
        }
    }

    bool ArmControl::move_pose(std::vector<double> pose, std::string command){
        auto request = std::make_shared<arm_interfaces::srv::ArmCommand::Request>();
        request->data = pose;
        request->command = command;
        auto result_future = move_pose_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Pose move success");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Pose move failed");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service move_pose");
            return false;
        }
    }

    bool ArmControl::move_gripper(std::vector<double> current, std::string command){
        auto request = std::make_shared<arm_interfaces::srv::ArmCommand::Request>();
        request->data = current;
        request->command = command;
        auto result_future = move_gripper_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Gripper move success");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Gripper move failed");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service move_gripper");
            return false;
        }
    }

    double ArmControl::calculate_face_to_angle(std::vector<double> face_pose){
        RCLCPP_INFO(node_->get_logger(), "Face pose: %f %f %f", face_pose[0], face_pose[1], face_pose[2]);
        //the face pose is in "camera" frame
        double angle;
        double x_diff = face_pose[0];
        double y_diff = face_pose[1];
        angle = atan2(y_diff, x_diff);
        //change to degree
        angle = angle * 180 / M_PI;
        if (angle >90){
            angle = (angle - 90)*(-1);
        }
        else if (angle < -90){
            angle = 90-angle;
        }

        RCLCPP_INFO(node_->get_logger(), "Turn face angle: %f", angle);

        return angle;
    }


    void ArmControl::register_nodes(BT::BehaviorTreeFactory &factory){
        factory.registerSimpleAction("MoveJoint", [&](TreeNode &node)
            {   auto angles = getInputValue<std::vector<double>>(node,"angles");
                auto command = getInputValue<std::string>(node,"command");
                return bool_to_node_status(move_joint(angles, command));},
            {InputPort<std::vector<double>>("angles"), InputPort<std::string>("command")});
        
        factory.registerSimpleAction("FaceTo", [&](TreeNode &node)
            {   auto face_pose = getInputValue<std::vector<double>>(node,"face_pose");
                auto angle = calculate_face_to_angle(face_pose);
                std::vector<double> move_angles={angle, 0.0, 0.0, 0.0, 0.0, 0.0};
                std::string command = "relative";
                return bool_to_node_status(move_joint(move_angles, command));},
            {InputPort<std::vector<double>>("face_pose"), InputPort<std::vector<double>>("robot_pose")});


        factory.registerSimpleAction("SetTarget", [&](TreeNode &node)
            {   auto pose = getInputValue<std::vector<double>>(node,"pose");
                auto command = getInputValue<std::string>(node,"command");
                return bool_to_node_status(set_target(pose, command));},
            {InputPort<std::vector<double>>("pose"), InputPort<std::string>("command")});

        factory.registerSimpleAction("MovePose", [&](TreeNode &node)
            {   auto pose = getInputValue<std::vector<double>>(node,"pose");
                auto command = getInputValue<std::string>(node,"command");
                return bool_to_node_status(move_pose(pose, command));},
            {InputPort<std::vector<double>>("pose"), InputPort<std::string>("command")});

        factory.registerSimpleAction("MoveGripper", [&](TreeNode &node)
            {   auto current = getInputValue<std::vector<double>>(node,"current");
                auto command = getInputValue<std::string>(node,"command");
                return bool_to_node_status(move_gripper(current, command));},
            {InputPort<std::vector<double>>("current"), InputPort<std::string>("command")});
    }
}