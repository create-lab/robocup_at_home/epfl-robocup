#include "behavior_tree/face_recognition_bt.hpp"

namespace behavior_tree
{
    /**
     * @brief Construct a new face recognition behavior tree Client:: FaceRecognition behavior tree Client object
     *
     * @param client_name The name of the node that is instantiated.
     */
    FaceRecognition::FaceRecognition(const std::string &client_name){
        node_ = rclcpp::Node::make_shared(client_name);

        get_face_recognition_client_ = node_->create_client<perception_interfaces::srv::GetFaceRecognition>("get_face_recognition");
        get_feature_recognition_client_ = node_->create_client<perception_interfaces::srv::GetPersonInfo>("get_feature_recognition");
        store_name_to_db_client_ = node_->create_client<perception_interfaces::srv::StoreNameToDb>("store_name_to_db");
        store_drink_to_db_client_ = node_->create_client<perception_interfaces::srv::StoreNameToDb>("store_drink_to_db");
        get_person_info_client_ = node_->create_client<perception_interfaces::srv::GetPersonInfo>("get_person_info");
        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(node_->get_clock());
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);


        while (!get_face_recognition_client_->wait_for_service(1s)|| !get_feature_recognition_client_->wait_for_service(1s)  || !store_name_to_db_client_->wait_for_service(1s)
        || !store_drink_to_db_client_->wait_for_service(1s) || !get_person_info_client_->wait_for_service(1s))
        {
            if (!rclcpp::ok()) {
                throw std::runtime_error("Interrupted while waiting for the service. Exiting.");
            }
            RCLCPP_INFO(node_->get_logger(), "%s service(s) not available, waiting again...", client_name.c_str());
        }
        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    std::tuple<BT::NodeStatus, std::string, geometry_msgs::msg::TransformStamped> FaceRecognition::get_face_recognition(){
        auto request = std::make_shared<perception_interfaces::srv::GetFaceRecognition::Request>();
        auto result_future = get_face_recognition_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
                {
            auto result = result_future.get();
            if (result->face_info.size() > 0){
                // Get the first face detected. First face should be the biggest face in the image. The faces are sorted by size
                std::string first_face_id = result->face_info[0].id;
                // std::vector<int> first_face_middle_point = result->face_bbx[0].x + result->face_bbx[0].w/2, result->face_bbx[0].y + result->face_bbx[0].h/2;
                geometry_msgs::msg::TransformStamped first_face_pose = result->face_transform[0];

                RCLCPP_INFO(node_->get_logger(), "Got face detected, first face id: %s", first_face_id.c_str());
                return std::make_tuple(BT::NodeStatus::SUCCESS, first_face_id,first_face_pose);
            }
            else{
                RCLCPP_INFO(node_->get_logger(), "Got no face detected");
                return std::make_tuple(BT::NodeStatus::FAILURE, "",geometry_msgs::msg::TransformStamped());
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_face_recognition");
            return std::make_tuple(BT::NodeStatus::FAILURE, "",geometry_msgs::msg::TransformStamped());
        }
    }

    std::tuple<BT::NodeStatus, std::string, geometry_msgs::msg::TransformStamped> FaceRecognition::find_face(std::string target_id){
        auto request = std::make_shared<perception_interfaces::srv::GetFaceRecognition::Request>();
        auto result_future = get_face_recognition_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->face_info.size() > 0){
                for(int i=0;i<result->face_info.size();i++){
                    if(result->face_info[i].id==target_id){
                        std::string face_id = result->face_info[i].id;
                        geometry_msgs::msg::TransformStamped face_pose = result->face_transform[i];
                        if (face_pose==geometry_msgs::msg::TransformStamped ()){
                            RCLCPP_INFO(node_->get_logger(), "Face pose is empty");
                            return std::make_tuple(BT::NodeStatus::RUNNING, "",geometry_msgs::msg::TransformStamped());
                        }
                        RCLCPP_INFO(node_->get_logger(), "Find person face id: %s", face_id.c_str());
                        return std::make_tuple(BT::NodeStatus::SUCCESS, face_id,face_pose);
                    }
                }
                RCLCPP_INFO(node_->get_logger(), "Do not find %s detected", target_id.c_str());
                return std::make_tuple(BT::NodeStatus::FAILURE, "",geometry_msgs::msg::TransformStamped());
            }
            else{
                RCLCPP_INFO(node_->get_logger(), "Got no face detected");
                return std::make_tuple(BT::NodeStatus::FAILURE, "",geometry_msgs::msg::TransformStamped());
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_face_recognition");
            return std::make_tuple(BT::NodeStatus::FAILURE, "",geometry_msgs::msg::TransformStamped());
        }
    }

    std::tuple<bool, geometry_msgs::msg::Pose> FaceRecognition::transform(geometry_msgs::msg::TransformStamped object_transform, std::string target_frame){
        geometry_msgs::msg::Pose pose = geometry_msgs::msg::Pose();
        // transform the object pose from camera frame to target_frame frame
        try
        {
            auto t = tf_buffer_->transform(object_transform, target_frame, tf2::durationFromSec(0.5));

            pose.position.x = t.transform.translation.x;
            pose.position.y = t.transform.translation.y;
            pose.position.z = t.transform.translation.z;
            pose.orientation.x = t.transform.rotation.x;
            pose.orientation.y = t.transform.rotation.y;
            pose.orientation.z = t.transform.rotation.z;
            pose.orientation.w = t.transform.rotation.w;

            RCLCPP_INFO(node_->get_logger(), "Transformed object from %s into %s frame",object_transform.child_frame_id.c_str(),target_frame.c_str());
        }
        catch (const tf2::TransformException &ex)
        {
            RCLCPP_INFO(node_->get_logger(), "Could not transform object from %s into %s frame: %s",object_transform.child_frame_id.c_str(),target_frame.c_str(), ex.what());
            return {false, geometry_msgs::msg::Pose()};
        }
        return {true, pose};
    }

    std::tuple<bool, std::vector<std::string>> FaceRecognition::get_feature_recognition(std::string id){
        auto request = std::make_shared<perception_interfaces::srv::GetPersonInfo::Request>();
        request->id = id;
        auto result_future = get_feature_recognition_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                auto person_info = result->face_info;
                std::vector<std::string> features = {person_info.gender, person_info.age, person_info.race, person_info.emotions};
                RCLCPP_INFO(node_->get_logger(), "Got feature recognition result. Person id %s gender: %s", person_info.id.c_str() ,person_info.gender.c_str());
                return std::make_tuple(true, features);
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Failed to get feature recognition result");
                return std::make_tuple(false, std::vector<std::string>{});
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_feature_recognition");
            return std::make_tuple(false, std::vector<std::string>{});
        }
    }


    /**
     * @brief Store the name of the person to the database
     *
     * @param names The name of the person 
     * @param id The id of the person
     * @return true If the name is stored successfully
     * @return false If the name is not stored successfully
     */
    bool FaceRecognition::store_name_to_db(std::string name, std::string id){
        auto request = std::make_shared<perception_interfaces::srv::StoreNameToDb::Request>();
        request->names = {name};
        request->id = id;
        auto result_future = store_name_to_db_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Stored name to database");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Failed to store name to database");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service store_name_to_db");
            return false;
        }
    }

    /**
     * @brief Store the drink to the database
     *
     * @param names The favorite drink of the person
     * @param id The id of the person
     * @return true If the name is stored successfully
     * @return false If the name is not stored successfully
     */
    bool FaceRecognition::store_drink_to_db(std::vector<std::string> drinks, std::string id){
        auto request = std::make_shared<perception_interfaces::srv::StoreNameToDb::Request>();
        request->names = {drinks};
        request->id = id;
        auto result_future = store_drink_to_db_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                RCLCPP_INFO(node_->get_logger(), "Stored drink to database");
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Failed to store drink to database");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service store_drink_to_db");
            return false;
        }
    }

    bool FaceRecognition::get_person_info(std::string id){
        auto request = std::make_shared<perception_interfaces::srv::GetPersonInfo::Request>();
        request->id = id;
        auto result_future = get_person_info_client_->async_send_request(request);

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
        {
            auto result = result_future.get();
            if (result->success){
                auto person_info = result->face_info;
                RCLCPP_INFO(node_->get_logger(), "Got person id %s info: %s", person_info.id.c_str() ,person_info.name.c_str());
                return true;
            }
            else{
                RCLCPP_ERROR(node_->get_logger(), "Failed to get person info");
                return false;
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service get_person_info");
            return false;
        }
    }

    void FaceRecognition::register_nodes(BT::BehaviorTreeFactory &factory){
        factory.registerSimpleAction("DoFaceRecognition", [&](TreeNode &node)
            {   auto [nodetsatus,id,face_pose]=get_face_recognition();
                node.setOutput("person_id", id);
                if (nodetsatus == BT::NodeStatus::FAILURE)
                    {return nodetsatus;}
                auto [is_success,pose] = transform(face_pose, "camera");
                std::vector<double> pose_vector = {pose.position.x, pose.position.y, pose.position.z, pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w};
                node.setOutput("face_pose_in_base", pose_vector);
                return nodetsatus;},
            {OutputPort<std::string>("person_id"),OutputPort<std::vector<double>>("face_pose_in_base")});
        
        factory.registerSimpleAction("DoFeatureRecognition", [&](TreeNode &node)
            {   auto id = getInputValue<std::string>(node,"person_id");
                auto [is_success, features] = get_feature_recognition(id);
                node.setOutput("face_feature", features);
                return bool_to_node_status(is_success);},
            {InputPort<std::string>("person_id"), OutputPort<std::vector<std::string>>("face_feature")});
        
        factory.registerSimpleAction("StoreNameToDb", [&](TreeNode &node)
            {   auto name = getInputValue<std::string>(node,"person_name");
                auto id = getInputValue<std::string>(node,"person_id");
                return bool_to_node_status(store_name_to_db(name, id));},
            {InputPort<std::string>("person_name"), InputPort<std::string>("person_id")});
        
        factory.registerSimpleAction("StoreDrinkToDb", [&](TreeNode &node)
            {   auto drinks = getInputValue<std::vector<std::string>>(node,"drinks");
                auto id = getInputValue<std::string>(node,"person_id");
                return bool_to_node_status(store_drink_to_db(drinks, id));},
            {InputPort<std::vector<std::string>>("drinks"), InputPort<std::string>("person_id")});
        
        factory.registerSimpleAction("GetPersonInfo", [&](TreeNode &node)
            {   auto id = getInputValue<std::string>(node,"person_id");
                return bool_to_node_status(get_person_info(id));},
            {InputPort<std::string>("person_id")});

        factory.registerSimpleAction("FindFace", [&](TreeNode &node)
            {   std::vector<double> pose_vector;
                auto target_person_id = getInputValue<std::string>(node,"person_id");
                auto target_frame = getInputValue<std::string>(node,"target_frame");
                auto [nodetsatus,id,face_pose]=find_face(target_person_id);
                if (nodetsatus == BT::NodeStatus::FAILURE)
                    {
                        node.setOutput("go_forward", false);
                        return nodetsatus;}
                else if (nodetsatus == BT::NodeStatus::RUNNING)
                    {
                        node.setOutput("go_forward", true);
                        return BT::NodeStatus::SUCCESS;
                    }
                RCLCPP_INFO(node_->get_logger(), "Transform face pose to map frame");
                if (target_frame!=""){
                auto [is_success,pose] = transform(face_pose, target_frame);
                    if (!is_success){
                        node.setOutput("go_forward", false);
                        return BT::NodeStatus::FAILURE;
                    }
                    std::vector<double> pose_vector = {pose.position.x, pose.position.y, pose.position.z, pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w};
                }
                else{
                    std::vector<double> pose_vector = {face_pose.transform.translation.x, face_pose.transform.translation.y, face_pose.transform.translation.z, face_pose.transform.rotation.x, face_pose.transform.rotation.y, face_pose.transform.rotation.z, face_pose.transform.rotation.w};
                }
                node.setOutput("face_pose_in_base", pose_vector);
                node.setOutput("go_forward", false);
                return nodetsatus;},
            {InputPort<std::string>("person_id"),InputPort<std::string>("target_frame"), OutputPort<std::vector<double>>("face_pose_in_base"), OutputPort<bool>("go_forward")});
    }
}