#include "behavior_tree/human_pose_estimation_bt.hpp"

namespace behavior_tree
{
    /**
     * @brief Construct a new human pose estimation behavior tree Client:: HumanPoseEstimation behavior tree Client object
     *
     * @param client_name The name of the node that is instantiated.
     */
    HumanPoseEstimation::HumanPoseEstimation(const std::string &client_name){
        node_ = rclcpp::Node::make_shared(client_name);

        sit_detection_client_ = node_->create_client<perception_interfaces::srv::SitDetect>("sit_detection");
        sit_infos_publisher_ = node_->create_publisher<perception_interfaces::msg::SitInfos>("sit_infos", 10);

        while (!sit_detection_client_->wait_for_service(1s))
        {
            if (!rclcpp::ok()) {
                throw std::runtime_error("Interrupted while waiting for the service. Exiting.");
            }
            RCLCPP_INFO(node_->get_logger(), "%s service(s) not available, waiting again...", client_name.c_str());
        }
        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    std::tuple<bool, bool> HumanPoseEstimation::sit_detection(){
        auto request = std::make_shared<perception_interfaces::srv::SitDetect::Request>();
        auto result_future = sit_detection_client_->async_send_request(request);
        RCLCPP_INFO(node_->get_logger(), "Start to call service sit_detection");

        if (rclcpp::spin_until_future_complete(node_->get_node_base_interface(), result_future) ==
        rclcpp::FutureReturnCode::SUCCESS)
                {
            auto result = result_future.get();
            // Only return the first skelenton result since it is the closest one
            if (result->is_detected){
                auto sit_results=result->result;
                for (auto sit_result : sit_results){
                    if (sit_result.is_success){
                        if (sit_result.is_sit){
                            RCLCPP_INFO(node_->get_logger(), "Person at %f %f %f is sitting", sit_result.person_position[0], sit_result.person_position[1], sit_result.person_position[2]);
                            return std::make_tuple(true, true);
                        }
                        else{
                            RCLCPP_INFO(node_->get_logger(), "Person at %f %f %f is not sitting", sit_result.person_position[0], sit_result.person_position[1], sit_result.person_position[2]);
                            return std::make_tuple(true, false);
                        }
                    }
                    else{
                        RCLCPP_INFO(node_->get_logger(), "Failed to detect sit or not");
                        return std::make_tuple(false, false);
                    }
                }
            }
            else{
                RCLCPP_INFO(node_->get_logger(), "Got no human skeleton detected");
                return std::make_tuple(false, false);
            }
        }
        else
        {
            RCLCPP_ERROR(node_->get_logger(), "Failed to call service sit_detection");
            return std::make_tuple(false, false);
        }
    }

    void HumanPoseEstimation::clean_sit_visualization(){
        sit_infos_publisher_->publish(perception_interfaces::msg::SitInfos());
    }

    void HumanPoseEstimation::register_nodes(BT::BehaviorTreeFactory &factory){
         factory.registerSimpleAction("FindSitPerson", [&](TreeNode &node)
            {   auto [is_success, is_sit]=sit_detection();
                return bool_to_node_status(is_sit);},
            {});
        factory.registerSimpleAction("CleanSitVisual", [&](TreeNode &node)
            {   clean_sit_visualization();
                return BT::NodeStatus::SUCCESS;},
            {});
    }
}