#include "behavior_tree/navigation_bt.hpp"

namespace behavior_tree
{
    /**
     * @brief Construct a new Navigation behavior tree Client:: Navigation behavior tree Client object
     *
     * @param client_name The name of the node that is instantiated.
     */
    NavBT::NavBT(const std::string &client_name){
        node_ = rclcpp::Node::make_shared(client_name);

        navigate_to_pose_action_client_ = rclcpp_action::create_client<nav2_msgs::action::NavigateToPose>(node_,"navigate_to_pose");
        base_velocity_publisher_ = node_->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
        tf_buffer_ = std::make_unique<tf2_ros::Buffer>(node_->get_clock());
        tf_listener_ = std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);

        // while (!navigate_to_pose_action_client_->wait_for_action_server(1s) || base_velocity_publisher_->get_subscription_count() == 0)
        // {
        //     if (!rclcpp::ok()) {
        //         throw std::runtime_error("Interrupted while waiting for the service. Exiting.");
        //     }
        //     RCLCPP_INFO(node_->get_logger(), "service(s) not available, waiting again...");
        // }
        RCLCPP_INFO(node_->get_logger(), "%s behavior tree client has been initialized.", client_name.c_str());
    }

    bool NavBT::send_goal_navigate_to_pose_action_(std::vector<double> pose){

        auto goal = nav2_msgs::action::NavigateToPose::Goal();
        auto send_pose= geometry_msgs::msg::PoseStamped();
        send_pose.pose.position.x = pose[0];
        send_pose.pose.position.y = pose[1];
        send_pose.pose.position.z = pose[2];
        send_pose.pose.orientation.x = pose[3];
        send_pose.pose.orientation.y = pose[4];
        send_pose.pose.orientation.z = pose[5];
        send_pose.pose.orientation.w = pose[6];

        send_pose.header.frame_id = "map";
        goal.pose = send_pose;
        
        RCLCPP_INFO(node_->get_logger(), "Sending goal");
        RCLCPP_INFO(node_->get_logger(), "Goal Pose: %f, %f, %f", send_pose.pose.position.x, send_pose.pose.position.y, send_pose.pose.position.z);
        RCLCPP_INFO(node_->get_logger(), "Goal Orientation: %f, %f, %f, %f", send_pose.pose.orientation.x, send_pose.pose.orientation.y, send_pose.pose.orientation.z, send_pose.pose.orientation.w);

        

        auto send_goal_options = rclcpp_action::Client<nav2_msgs::action::NavigateToPose>::SendGoalOptions();
        send_goal_options.goal_response_callback = std::bind(&NavBT::goal_response_callback_navigate_to_pose_action_, this, std::placeholders::_1);
        send_goal_options.feedback_callback = std::bind(&NavBT::feedback_callback_navigate_to_pose_action_, this, std::placeholders::_1, std::placeholders::_2);
        send_goal_options.result_callback = std::bind(&NavBT::result_callback_navigate_to_pose_action_, this, std::placeholders::_1);
        navigate_to_pose_action_client_->async_send_goal(goal, send_goal_options);
        

        // Spin until the condition is met (e.g., goal completed or timeout)
        // rclcpp::Rate loop_rate(10); // Adjust the rate as needed
        while (rclcpp::ok()) {
            if (send_goal_success == BT::NodeStatus::FAILURE) {
                return false;
            }
            else if (send_goal_success == BT::NodeStatus::SUCCESS) {
                if (goal_complete == BT::NodeStatus::SUCCESS) {
                    return true;
                }
                else if (goal_complete == BT::NodeStatus::FAILURE) {
                    return false;
                }
            }
           
            // Spin once (non-blocking)
            rclcpp::spin_some(node_->get_node_base_interface());
            // loop_rate.sleep();
        }
        return false;
    }

    

    void NavBT::goal_response_callback_navigate_to_pose_action_(const rclcpp_action::ClientGoalHandle<nav2_msgs::action::NavigateToPose>::SharedPtr & goal_handle){
        if (!goal_handle) {
            RCLCPP_INFO(node_->get_logger(), "Goal was rejected by server");
            send_goal_success = BT::NodeStatus::FAILURE;
        }
        else{
            RCLCPP_INFO(node_->get_logger(), "Goal accepted by server, waiting for result");
            send_goal_success = BT::NodeStatus::SUCCESS;
        }
        
    }


    void NavBT::feedback_callback_navigate_to_pose_action_(
        rclcpp_action::ClientGoalHandle<nav2_msgs::action::NavigateToPose>::SharedPtr,
        const std::shared_ptr<const nav2_msgs::action::NavigateToPose::Feedback> feedback)
    {
        auto current_pose = feedback->current_pose;
        auto navigation_time = feedback->navigation_time.sec;
        auto estimated_time_remaining = feedback->estimated_time_remaining.sec;
        auto distance_remaining=feedback->distance_remaining;

        RCLCPP_INFO(node_->get_logger(), "Current Pose: %f, %f, %f", current_pose.pose.position.x, current_pose.pose.position.y, current_pose.pose.position.z);
        RCLCPP_INFO(node_->get_logger(), "Navigation Time: %d", navigation_time);
        RCLCPP_INFO(node_->get_logger(), "Estimated Time Remaining: %d", estimated_time_remaining);
        RCLCPP_INFO(node_->get_logger(), "Distance Remaining: %f", distance_remaining);
    }




    void NavBT::result_callback_navigate_to_pose_action_(const rclcpp_action::ClientGoalHandle<nav2_msgs::action::NavigateToPose>::WrappedResult & result){
        if (result.code == rclcpp_action::ResultCode::SUCCEEDED) {
            RCLCPP_INFO(node_->get_logger(), "navigate_to_pose: Goal was successfully completed.");
            goal_complete = BT::NodeStatus::SUCCESS;
        }
        else if (result.code == rclcpp_action::ResultCode::ABORTED) {
            RCLCPP_INFO(node_->get_logger(), "navigate_to_pose: Goal was aborted");
            goal_complete = BT::NodeStatus::FAILURE;
        }
        else if (result.code == rclcpp_action::ResultCode::CANCELED) {
            RCLCPP_INFO(node_->get_logger(), "navigate_to_pose: Goal was canceled");
            goal_complete = BT::NodeStatus::FAILURE;
        }
        else {
            RCLCPP_INFO(node_->get_logger(), "navigate_to_pose: Unknown result code");
            goal_complete = BT::NodeStatus::FAILURE;
        }
        return;
    }

    std::tuple<bool,geometry_msgs::msg::Pose> NavBT::get_robot_posiiton_in_map(){
        geometry_msgs::msg::TransformStamped robot_transform;
        geometry_msgs::msg::Pose robot_position_in_map_;
        try
        {
            robot_transform = tf_buffer_->lookupTransform("map", "base_link", tf2::TimePointZero); //TODO: not sure is base_footprint or base_link
            robot_position_in_map_.position.x = robot_transform.transform.translation.x;
            robot_position_in_map_.position.y = robot_transform.transform.translation.y;
            robot_position_in_map_.position.z = robot_transform.transform.translation.z;
            robot_position_in_map_.orientation.x = robot_transform.transform.rotation.x;
            robot_position_in_map_.orientation.y = robot_transform.transform.rotation.y;
            robot_position_in_map_.orientation.z = robot_transform.transform.rotation.z;
            robot_position_in_map_.orientation.w = robot_transform.transform.rotation.w;

            RCLCPP_INFO(node_->get_logger(), "Robot position in map: %f, %f, %f, %f, %f, %f, %f", robot_position_in_map_.position.x, robot_position_in_map_.position.y, robot_position_in_map_.position.z, robot_position_in_map_.orientation.x, robot_position_in_map_.orientation.y, robot_position_in_map_.orientation.z, robot_position_in_map_.orientation.w);

            return {true,robot_position_in_map_};

        }
        catch (const tf2::TransformException &ex)
        {
            RCLCPP_INFO(node_->get_logger(), "Could not get transform from map to base_link: %s", ex.what());

            return {false, geometry_msgs::msg::Pose()};
        }
    }

    std::vector<double> NavBT::go_to_person_pose(std::vector<double> face_pose, std::vector<double> robot_pose, double distance=1.0){
        // distance: the distance between the robot goal point and person in meter
        RCLCPP_INFO(node_->get_logger(), "Get pose with some distance");
        std::array<double, 3> vec = {face_pose[0] - robot_pose[0], face_pose[1] - robot_pose[1], face_pose[2] - robot_pose[0]};
        double norm = std::sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);
        std::array<double, 3> norm_vec = {distance* vec[0] / norm, distance * vec[1] / norm, distance * vec[2] / norm};

        std::array<double, 3> goal_position = {face_pose[0] - norm_vec[0], face_pose[1] - norm_vec[1], robot_pose[2]};
        double theta = std::atan2(goal_position[1], goal_position[0]);
        std::array<double, 4> goal_orientation = {0.0, 0.0, std::sin(theta / 2), std::cos(theta / 2)};


        std::vector<double> pose= {goal_position[0], goal_position[1], goal_position[2], goal_orientation[0], goal_orientation[1], goal_orientation[2], goal_orientation[3]};

        RCLCPP_INFO(node_->get_logger(), "Goal pose with some distance: %f, %f, %f, %f, %f, %f, %f", pose[0], pose[1], pose[2], pose[3], pose[4], pose[5], pose[6]);

        return pose;
    }


    void NavBT::rotate() {
        RCLCPP_INFO(node_->get_logger(), "Rotating the robot");
        //publish the topic in cmd_vel to rotate the robot
        geometry_msgs::msg::Twist msg;
        msg.angular.z = 0.3;
        base_velocity_publisher_->publish(msg);

        sleep(2);

        //stop the robot
        msg.angular.z = 0;
        base_velocity_publisher_->publish(msg);
    }

    void NavBT::forward() {
        RCLCPP_INFO(node_->get_logger(), "Robot forward");
        //publish the topic in cmd_vel to let the robot go forward
        geometry_msgs::msg::Twist msg;
        msg.linear.x = 0.2;
        base_velocity_publisher_->publish(msg);

        sleep(5);

        //stop the robot
        msg.linear.x = 0;
        base_velocity_publisher_->publish(msg);
    }

    void NavBT::register_nodes(BT::BehaviorTreeFactory &factory){
        factory.registerSimpleAction("GoTo", [&](TreeNode &node)
            {   auto pose = getInputValue<std::vector<double>>(node, "base_target");
                auto is_success=send_goal_navigate_to_pose_action_(pose);
                send_goal_success=BT::NodeStatus::RUNNING;
                goal_complete=BT::NodeStatus::RUNNING;
                return bool_to_node_status(is_success); },
                {InputPort<std::vector<double>>("base_target")});

        factory.registerSimpleAction("GetRobotPosition", [&](TreeNode &node)
            {   auto [is_success, robot_position]=get_robot_posiiton_in_map();
                std::vector<double> pose_vector= {robot_position.position.x, robot_position.position.y, robot_position.position.z, robot_position.orientation.x, robot_position.orientation.y, robot_position.orientation.z, robot_position.orientation.w};
                node.setOutput("robot_position", pose_vector);
                return bool_to_node_status(is_success); },
                {OutputPort<std::vector<double>>("robot_position")});
        
        factory.registerSimpleAction("RobotRotate", [&](TreeNode &node)
            {   rotate();
                return BT::NodeStatus::SUCCESS; },
                {});
        factory.registerSimpleAction("RobotForward", [&](TreeNode &node)
            {   bool go_forward=getInputValue<bool>(node, "go_forward");
                if (!go_forward){
                    return BT::NodeStatus::SUCCESS;
                }
                forward();
                return BT::NodeStatus::SUCCESS; },
                {InputPort<bool>("go_forward")});
        factory.registerSimpleAction("GoToWithDistance", [&](TreeNode &node)
            {   auto target_pose = getInputValue<std::vector<double>>(node, "base_target");
                auto distance = getInputValue<double>(node, "distance");
                auto [is_success, robot_position]=get_robot_posiiton_in_map();
                if (!is_success){
                    RCLCPP_INFO(node_->get_logger(), "Failed to get robot position in map");
                    return BT::NodeStatus::FAILURE;
                }
                std::vector<double> robot_pose= {robot_position.position.x, robot_position.position.y, robot_position.position.z, robot_position.orientation.x, robot_position.orientation.y, robot_position.orientation.z, robot_position.orientation.w};
                auto final_target_pose=go_to_person_pose(target_pose, robot_pose, distance);
                return bool_to_node_status(send_goal_navigate_to_pose_action_(final_target_pose)); },
                {InputPort<std::vector<double>>("base_target"), InputPort<std::vector<double>>("distance")});
    }

}