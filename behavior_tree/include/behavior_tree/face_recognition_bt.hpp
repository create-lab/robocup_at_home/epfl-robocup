#ifndef __FACE_RECOGNITION_BT_HPP__
#define __FACE_RECOGNITION_BT_HPP__

#include "rclcpp/rclcpp.hpp"

#include <ament_index_cpp/get_package_share_directory.hpp>

#include "perception_interfaces/srv/get_face_recognition.hpp"
#include "perception_interfaces/srv/store_name_to_db.hpp"
#include "perception_interfaces/srv/get_person_info.hpp"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2/exceptions.h"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"

#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include <csignal>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the face reocgnition
     *
     */
    class FaceRecognition
    {
    private:
        rclcpp::Node::SharedPtr node_;
        rclcpp::Client<perception_interfaces::srv::GetFaceRecognition>::SharedPtr get_face_recognition_client_;
        rclcpp::Client<perception_interfaces::srv::GetPersonInfo>::SharedPtr get_feature_recognition_client_;
        rclcpp::Client<perception_interfaces::srv::StoreNameToDb>::SharedPtr store_name_to_db_client_;
        rclcpp::Client<perception_interfaces::srv::StoreNameToDb>::SharedPtr store_drink_to_db_client_;
        rclcpp::Client<perception_interfaces::srv::GetPersonInfo>::SharedPtr get_person_info_client_;
        std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
        std::unique_ptr<tf2_ros::Buffer> tf_buffer_;

    public:
        FaceRecognition(const std::string &client_name);

    public:
        std::tuple<BT::NodeStatus, std::string, geometry_msgs::msg::TransformStamped> get_face_recognition();
        std::tuple<bool, std::vector<std::string>> get_feature_recognition(std::string id);
        std::tuple<bool, geometry_msgs::msg::Pose> transform(geometry_msgs::msg::TransformStamped object_transform,std::string target_frame);
        bool store_name_to_db(std::string name, std::string id);
        bool store_drink_to_db(std::vector<std::string> drinks, std::string id);
        void register_nodes(BT::BehaviorTreeFactory &factory);
        bool get_person_info(std::string id);
        std::tuple<BT::NodeStatus, std::string, geometry_msgs::msg::TransformStamped> find_face(std::string target_id);
       
    };
}
#endif // __FACE_RECOGNITION_BT_HPP__