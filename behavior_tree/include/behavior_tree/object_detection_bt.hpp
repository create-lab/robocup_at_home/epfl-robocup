#ifndef __OBJECT_DETECTION_BT_HPP__
#define __OBJECT_DETECTION_BT_HPP__

#include "rclcpp/rclcpp.hpp"
#include <ament_index_cpp/get_package_share_directory.hpp>
#include "perception_interfaces/srv/get_object_pose.hpp"
#include "perception_interfaces/srv/get_gripper_angle.hpp"
#include "perception_interfaces/srv/get_object_info.hpp"
#include "perception_interfaces/srv/get_point_pose.hpp"
#include "perception_interfaces/msg/object_info.hpp"
#include "perception_interfaces/msg/object_infos.hpp"
#include "perception_interfaces/msg/object_names.hpp"
#include "std_msgs/msg/string.hpp"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2/exceptions.h"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"
#include <map>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to get object detection
     */
    class ObjectDetectionBT
    {
    private:
        rclcpp::Node::SharedPtr node_;
        std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
        std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
        rclcpp::Client<perception_interfaces::srv::GetObjectInfo>::SharedPtr get_object_info_client_;
        rclcpp::Client<perception_interfaces::srv::GetObjectPose>::SharedPtr get_object_pose_client_;
        rclcpp::Client<perception_interfaces::srv::GetGripperAngle>::SharedPtr get_gripper_angle_client_;
        rclcpp::Publisher<perception_interfaces::msg::ObjectNames>::SharedPtr object_name_publisher_;
        rclcpp::Client<perception_interfaces::srv::GetPointPose>::SharedPtr get_point_pose_client_;
        rclcpp::Publisher<perception_interfaces::msg::ObjectInfos>::SharedPtr empty_seats_publisher_;
        std::map<std::string, std::vector<perception_interfaces::msg::ObjectInfo>> object_infos_dict_;
        std::vector<perception_interfaces::msg::ObjectInfo> empty_seats;

    public:
        ObjectDetectionBT(const std::string &client_name);

    public:
        std::tuple<bool, std::vector<perception_interfaces::msg::ObjectInfo>> get_object_info(std::string object_name);
        std::tuple<bool, geometry_msgs::msg::Pose> get_object_pose(std::string object_name, std::string target_frame);
        std::tuple<bool, double> get_gripper_angle(std::string object_name);
        void publish_object_name(std::vector<std::string> object_names);
        std::tuple<bool, geometry_msgs::msg::Pose> get_point_pose(std::vector<double> point,std::string object_name, std::string target_frame);
        std::tuple<bool,geometry_msgs::msg::Pose> find_empty_seat(std::string target_frame);
        void find_empty_chair(
            std::vector<perception_interfaces::msg::ObjectInfo> person_infos,
            std::vector<perception_interfaces::msg::ObjectInfo> chair_infos);
        void find_empty_couch(
            std::vector<perception_interfaces::msg::ObjectInfo> person_infos,
            std::vector<perception_interfaces::msg::ObjectInfo> couch_infos) ;
        float calculate_overlap(const std::vector<int16_t>& person_bbox, const std::vector<int16_t>& seat_bbox);
        std::tuple<bool, geometry_msgs::msg::Pose> transform(geometry_msgs::msg::TransformStamped object_transform, std::string target_frame);
        void clean_empty_seats_visualization();
        
        void register_nodes(BT::BehaviorTreeFactory &factory);
        
    };
}
#endif // __OBJECT_DETECTION_BT_HPP__
