#ifndef __NAV_BT_HPP__
#define __NAV_BT_HPP__

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "rclcpp_components/register_node_macro.hpp"

#include <ament_index_cpp/get_package_share_directory.hpp>

#include "nav2_msgs/action/navigate_to_pose.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"
#include "tf2_ros/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2/exceptions.h"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"

#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include <csignal>
#include <cmath>
#include <vector>
#include <array>
#include <Eigen/Dense>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the robot's navigation.
     *
     *
     */
    class NavBT
    {
    private:
        BT::NodeStatus send_goal_success=BT::NodeStatus::RUNNING;
        BT::NodeStatus goal_complete=BT::NodeStatus::RUNNING;

        rclcpp::Node::SharedPtr node_;
        std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
        std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
        rclcpp_action::Client<nav2_msgs::action::NavigateToPose>::SharedPtr navigate_to_pose_action_client_;
        rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr base_velocity_publisher_;

    public:
        NavBT(const std::string &client_name);

    public:
        bool send_goal_navigate_to_pose_action_(std::vector<double> pose);
        void goal_response_callback_navigate_to_pose_action_(const rclcpp_action::ClientGoalHandle<nav2_msgs::action::NavigateToPose>::SharedPtr & goal_handle);
        void feedback_callback_navigate_to_pose_action_(rclcpp_action::ClientGoalHandle<nav2_msgs::action::NavigateToPose>::SharedPtr,const std::shared_ptr<const nav2_msgs::action::NavigateToPose::Feedback> feedback);
        void result_callback_navigate_to_pose_action_(const rclcpp_action::ClientGoalHandle<nav2_msgs::action::NavigateToPose>::WrappedResult & result);
        std::tuple<bool, geometry_msgs::msg::Pose> transform(geometry_msgs::msg::TransformStamped object_transform, std::string target_frame);
        std::tuple<bool,geometry_msgs::msg::Pose> get_robot_posiiton_in_map();
        std::vector<double> go_to_person_pose(std::vector<double> face_pose, std::vector<double> robot_pose, double distance);
        void rotate();
        void forward();
        void register_nodes(BT::BehaviorTreeFactory &factory);
        
    };
}
#endif // __NAV_BT_HPP__
