#ifndef __HUMAN_POSE_ESTIMATION_BT_HPP__
#define __HUMAN_POSE_ESTIMATION_BT_HPP__

#include "rclcpp/rclcpp.hpp"

#include <ament_index_cpp/get_package_share_directory.hpp>

#include "perception_interfaces/srv/sit_detect.hpp"
#include "perception_interfaces/msg/sit_info.hpp"
#include "perception_interfaces/msg/sit_infos.hpp"
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"

#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include <csignal>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the face reocgnition
     *
     */
    class HumanPoseEstimation
    {
    private:
        rclcpp::Node::SharedPtr node_;
        rclcpp::Client<perception_interfaces::srv::SitDetect>::SharedPtr sit_detection_client_;
        rclcpp::Publisher<perception_interfaces::msg::SitInfos>::SharedPtr sit_infos_publisher_;

    public:
        HumanPoseEstimation(const std::string &client_name);

    public:
        std::tuple<bool, bool> sit_detection();
        void clean_sit_visualization();
        void register_nodes(BT::BehaviorTreeFactory &factory);
       
    };
}
#endif // __HUMAN_POSE_ESTIMATION_BT_HPP__