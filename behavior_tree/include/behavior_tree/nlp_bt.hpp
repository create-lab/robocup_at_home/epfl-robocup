#ifndef __NLP_BT_HPP__
#define __NLP_BT_HPP__

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "rclcpp_components/register_node_macro.hpp"

#include <ament_index_cpp/get_package_share_directory.hpp>

#include "std_msgs/msg/string.hpp"
#include "nlp_brain_interface/srv/say.hpp"
#include "nlp_brain_interface/action/get_name.hpp"
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"

#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include <csignal>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the nlp,
     * audio and microphone.
     *
     */
    class NLPBT
    {
    private:
        std::string feedback_ = "";
        BT::NodeStatus send_goal_success=BT::NodeStatus::RUNNING;
        std::tuple<BT::NodeStatus, std::vector<std::string>> send_goal_result=std::make_tuple(BT::NodeStatus::RUNNING, std::vector<std::string>{});

        rclcpp::Node::SharedPtr node_;
        rclcpp::Client<nlp_brain_interface::srv::Say>::SharedPtr say_client_;
        rclcpp_action::Client<nlp_brain_interface::action::GetName>::SharedPtr ask_name_or_drink_action_;

        //an example for sending two requests at "same" time, creating parallel actions
        BT::NodeStatus test_result=BT::NodeStatus::RUNNING;

    public:
        NLPBT(const std::string &client_name);

    public:
        bool robot_say(std::string text);
        bool send_goal_ask_name_or_drink_action_(std::string question, std::string ask_for);
        void goal_response_callback_ask_name_or_drink_action_(const rclcpp_action::ClientGoalHandle<nlp_brain_interface::action::GetName>::SharedPtr & goal_handle);
        void feedback_callback_ask_name_or_drink_action_(rclcpp_action::ClientGoalHandle<nlp_brain_interface::action::GetName>::SharedPtr,const std::shared_ptr<const nlp_brain_interface::action::GetName::Feedback> feedback);
        void result_callback_ask_name_or_drink_action_(const rclcpp_action::ClientGoalHandle<nlp_brain_interface::action::GetName>::WrappedResult & result);
        bool check_person_name(std::tuple<BT::NodeStatus, std::vector<std::string>> result);
        bool check_drink_name(std::tuple<BT::NodeStatus, std::vector<std::string>> result);
        std::string get_person_name(bool check_result);
        std::vector<std::string> get_drink_name(bool check_result);
        bool introduce_guest(std::string name, std::vector<std::string> drinks,bool is_first, std::vector<std::string> features);
        void register_nodes(BT::BehaviorTreeFactory &factory);
        
        
        //an example for sending two requests at "same" time, creating parallel actions
        bool example_parallel_action(std::string text);
        void say_robot_callback(const rclcpp::Client<nlp_brain_interface::srv::Say>::SharedFuture future);
    };
}
#endif // __NLP_BT_HPP__
