#ifndef __ARM_BT_HPP__
#define __ARM_BT_HPP__

#include "rclcpp/rclcpp.hpp"

#include <ament_index_cpp/get_package_share_directory.hpp>

#include "arm_interfaces/srv/arm_command.hpp"
// #include "tf2_ros/transform_listener.h"
// #include "tf2_ros/buffer.h"
// #include "tf2/exceptions.h"
// #include "geometry_msgs/msg/transform_stamped.hpp"
// #include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"

#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include <csignal>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control xarm6 robot arm.
     *
     */
    class ArmControl
    {
    private:
        rclcpp::Node::SharedPtr node_;
        rclcpp::Client<arm_interfaces::srv::ArmCommand>::SharedPtr move_joint_client_;
        rclcpp::Client<arm_interfaces::srv::ArmCommand>::SharedPtr set_target_from_cam_client_;
        rclcpp::Client<arm_interfaces::srv::ArmCommand>::SharedPtr move_pose_client_;
        rclcpp::Client<arm_interfaces::srv::ArmCommand>::SharedPtr move_gripper_client_;
        double current_arm_angle[6] = {-1.7,-64,-13,-1.1,-15.9,0.0}; //degree
        
    public:
        ArmControl(const std::string &client_name);

    public:
        bool move_joint(std::vector<double> angles, std::string command);
        bool set_target(std::vector<double> pose, std::string command);
        bool move_pose(std::vector<double> pose, std::string command);
        bool move_gripper(std::vector<double> current, std::string command);
        double calculate_face_to_angle(std::vector<double> face_pose);
        void register_nodes(BT::BehaviorTreeFactory &factory);
       
    };
}
#endif // __ARM_BT_HPP__