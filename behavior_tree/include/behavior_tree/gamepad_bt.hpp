#ifndef __GAMEPAD_BT_HPP__
#define __GAMEPAD_BT_HPP__

// TODO: The code do not use in competition so it is not test either.

#include "rclcpp/rclcpp.hpp"

#include <ament_index_cpp/get_package_share_directory.hpp>

#include "arm_interfaces/srv/arm_command.hpp"
// #include "tf2_ros/transform_listener.h"
// #include "tf2_ros/buffer.h"
// #include "tf2/exceptions.h"
// #include "geometry_msgs/msg/transform_stamped.hpp"
// #include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "behaviortree_cpp_v3/bt_factory.h"
#include "behavior_tree/bt_node_utils.hpp"

#include <functional>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <tuple>
#include <csignal>

using namespace std::chrono_literals;

namespace behavior_tree
{
    /**
     * @brief A class that instantiates a ROS 2 node that provides clients to the services used to control the gamepad.
     *
     */
    class Gamepad
    {
    private:
        rclcpp::Node::SharedPtr node_;
        rclcpp::Client<arm_interfaces::srv::ArmCommand>::SharedPtr is_button_pressed_client_;
        
    public:
        Gamepad(const std::string &client_name);

    public:
        bool is_button_press(std::string command);
        void register_nodes(BT::BehaviorTreeFactory &factory);
       
    };
}
#endif // __GAMEPAD_BT_HPP__