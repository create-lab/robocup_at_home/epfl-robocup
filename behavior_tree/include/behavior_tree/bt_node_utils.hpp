#ifndef __BT_NODE_UTILS_HPP__
#define __BT_NODE_UTILS_HPP__

#include "behaviortree_cpp_v3/behavior_tree.h"

namespace behavior_tree
{
    using namespace BT;
    NodeStatus bool_to_node_status(bool status)
    {
        if (status)
        {
            return NodeStatus::SUCCESS;
        }
        else
        {
            return NodeStatus::FAILURE;
        }
    }

    std::string toStr(NodeStatus status) {
        // Enumerates the states every node can be in after execution during a particular time step. IMPORTANT: Your custom nodes should NEVER return IDLE. 
        switch (status) {
            case NodeStatus::SUCCESS: return "SUCCESS";
            case NodeStatus::FAILURE: return "FAILURE";
            case NodeStatus::RUNNING: return "RUNNING";
            case NodeStatus::IDLE: return "IDLE";
            default: return "UNKNOWN";
        }
    }

    template <typename T>
    T getInputValue(const BT::TreeNode &node, const std::string &port_name)
    {
        Optional<T> msg = node.getInput<T>(port_name);
        // Check if optional is valid. If not, throw its error
        if (!msg)
        {
            throw BT::RuntimeError("missing required input [message]: ",
                                   msg.error());
        }

        return msg.value();
    }

    
}



namespace BT
{
    template <>
    inline std::vector<double> convertFromString(StringView str)
    {
        auto parts = splitString(str, ',');
        std::vector<double> output;
        for (const auto &p : parts)
        {
            output.push_back(convertFromString<double>(p));
        }
        return output;
    }

    template <>
    inline std::vector<std::string> convertFromString(StringView str)
    {
        auto parts = splitString(str, ',');
        std::vector<std::string> output;
        for (const auto &p : parts)
        {
            output.push_back(convertFromString<std::string>(p));
        }
        return output;
    }

    template <>
    inline std::map<std::string, std::vector<double>> convertFromString(StringView str)
    {
        auto parts = splitString(str, ';');
        std::map<std::string, std::vector<double>> output;
        for (const auto &p : parts)
        {
            auto sub_parts = splitString(p, ':');
            output.insert(std::pair<std::string, std::vector<double>>(sub_parts[0], convertFromString<std::vector<double>>(sub_parts[1])));
        }
        return output;
    }

    template <>
    inline std::map<std::string, std::string> convertFromString(StringView str)
    {
        auto parts = splitString(str, ',');
        std::map<std::string, std::string> output;
        for (const auto &p : parts)
        {
            auto sub_parts = splitString(p, ':');
            output.insert(std::pair<std::string, std::string>(sub_parts[0], convertFromString<std::string>(sub_parts[1])));
        }
        return output;
    }

    template <>
    inline std::map<std::string, int> convertFromString(StringView str)
    {
        auto parts = splitString(str, ',');
        std::map<std::string, int> output;
        for (const auto &p : parts)
        {
            auto sub_parts = splitString(p, ':');
            output.insert(std::pair<std::string, int>(sub_parts[0], convertFromString<int>(sub_parts[1])));
        }
        return output;
    }

    template <>
    inline bool convertFromString(StringView str)
    {
        if (str == "true" || str == "1")
        {
            return true;
        }
        else if (str == "false" || str == "0")
        {
            return false;
        }
        else
        {
            throw std::invalid_argument("Invalid string for bool conversion: " + std::string(str));
        }
    }
}

#endif // __BT_NODE_UTILS_HPP__