from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration

def generate_launch_description():
    logger = LaunchConfiguration('log_level', default='info')

    return LaunchDescription([
        DeclareLaunchArgument(
            'log_level',
            default_value='info',
            description='Logging level'
        ),

        Node(
            package='face_recognition',
            executable='face_recognition',
            name='face_recognition',
            arguments=['--ros-args', '--log-level', logger]
        ),
        # Node(
        #     package='human_pose_estimation',
        #     executable='human_pose_estimation',
        #     name='human_pose_estimation',
        #     arguments=['--ros-args', '--log-level', logger]
        # ),
        Node(
            package='nlp_brain',
            executable='nlp',
            name='nlp',
            arguments=['--ros-args', '--log-level', logger]
        ),
        Node(
            package='nlp_brain',
            executable='stt',
            name='stt',
            arguments=['--ros-args', '--log-level', logger]
        ),
        Node(
            package='visualization',
            executable='visualization',
            name='visualization',
            arguments=['--ros-args', '--log-level', logger]
        ),

        Node(
            package='xarm_control',
            executable='xarm_control_node',
            name='xarm_control',
            arguments=['--ros-args', '--log-level', logger]
        ),

        # Node(
        #     package='tf2_ros',
        #     executable='static_transform_publisher',
        #     arguments=[
        #         '0.47', '0.04', '0.48', '0', '0', '0', '1',
        #         'base_footprint', 'camera',
        #         '--ros-args', '--log-level', logger
        #     ]
        # ),
    ])
