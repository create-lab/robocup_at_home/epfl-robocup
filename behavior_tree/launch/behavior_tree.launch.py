from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration

def generate_launch_description():
    logger = LaunchConfiguration('log_level', default='info')
    return LaunchDescription([
        DeclareLaunchArgument(
            'log_level',
            default_value='info',
            description='Logging level'
        ),

        Node(
            package='behavior_tree',
            # namespace='nlp_brain',
            executable='main_bt',
            name='main_bt',
            arguments=['--ros-args', '--log-level', logger]
        ),
    ])
