import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2
from perception_interfaces.msg import ObjectInfos, SitInfos
from zed_interfaces.msg import ObjectsStamped
import numpy as np
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy

class Visualization(Node):

    def __init__(self):
        super().__init__('visualization')


        self.subscription_visual_object_detection = self.create_subscription(Image,'visualisation_object_detection',self.listener_visual_object_detection_callback,10)
        self.subscription_visual_face_recognition = self.create_subscription(Image,'visualization_face_recognition',self.listener_visual_face_recognition_callback,10)
        self.subscription_empty_seats=self.create_subscription(ObjectInfos,'empty_seats',self.listener_empty_seats_callback,10)
        # self.subscription_skeleton = self.create_subscription(ObjectsStamped,'/zedm/zed_node/body_trk/skeletons',  self.listener_skeleton_callback,10)
        # self.subscription_image = self.create_subscription(Image,'/zedm/zed_node/left/image_rect_color',  self.listener_image_callback,10)
        # print("Subscribed now to /zed/zed_node/left/image_rect_color")
        self.subscription_sit_infos = self.create_subscription(SitInfos,'sit_infos',self.listener_sit_infos_callback,10)
        self.cv_bridge = CvBridge()
        self.empty_seats_boxes=[]
        self.skeleton_data=None
        self.sit_infos=None


    def listener_visual_object_detection_callback(self, msg):
        image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
        if self.empty_seats_boxes != []:
            empty_seats_boxes=self.empty_seats_boxes
            image=self.draw_empty_seats(image,empty_seats_boxes)
            
        # print("Image shape:", image.shape)
        cv2.imshow("Object detection", image)
        cv2.waitKey(1)
    
    def listener_visual_face_recognition_callback(self, msg):
        image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
        # print("Image shape:", image.shape)
        cv2.imshow("Face recognition", image)
        cv2.waitKey(1)
    
    def listener_empty_seats_callback(self,msg):
        empty_seats=msg.object_infos
        empty_seats_boxes=[]
        for empty_seat in empty_seats:
            empty_seats_boxes.append(np.array(empty_seat.object_box))
        self.empty_seats_boxes=empty_seats_boxes
    
    def listener_image_callback(self, msg):

        image_data = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
        width = int(image_data.shape[1] * 2)
        height = int(image_data.shape[0] * 2)
        dim = (width, height)

        if self.skeleton_data != None:
            skeleton_data=self.skeleton_data.copy()
            self.draw_skeleton_on_image(image_data, skeleton_data)
            self.skeleton_data=None
        if self.sit_infos != None:
            sit_infos=self.sit_infos.copy()
            for sit_info in sit_infos:
                if sit_info.is_success:
                    x_center=int(sit_info.person_position[3])
                    y_center=int(sit_info.person_position[4])
                    if sit_info.is_sit:
                        cv2.putText(image_data, "Sit", (x_center+10, y_center), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)
                    else:
                        cv2.putText(image_data, "Stand", (x_center+10, y_center), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
        
        # resize image
        # resized = cv2.resize(image_data, dim, interpolation = cv2.INTER_AREA)
        
        # Display the image with the skeleton
        cv2.imshow('Human Pose Estimation', image_data)
        cv2.waitKey(1)
            
    
    def listener_skeleton_callback(self, msg):
        skeleton_data=[]
        objects=msg.objects
        for object in objects:
            skeleton_data_per_object=[]
            kps=object.skeleton_2d.keypoints
            for kp_one in kps[:18]:
                skeleton_data_per_object.append(kp_one.kp/2)
            skeleton_data.append(skeleton_data_per_object)

        self.skeleton_data=skeleton_data
    
    def listener_sit_infos_callback(self, msg):
        self.sit_infos=msg.sitinfos

    def draw_empty_seats(self,image, empty_seats_boxes):
        #empty seat box is x1, y1, x2, y2
        # Draw all boxes in green except the last one
        # print(empty_seats_boxes)
        for empty_seat in empty_seats_boxes[:-1]:
            if (empty_seat==empty_seats_boxes[-1]).all():
                # print("pass final empty seat")
                continue
            x1,y1,x2,y2=np.array(empty_seat).astype(int)
            cv2.rectangle(image, (x1, y1), (x2, y2), (0, 255, 0), 2)
            cv2.putText(image, "Empty seat", (x1, y1-10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 0), 1)
        # Draw the last box in red. Last box is the final empty seat to show the guest
        x1,y1,x2,y2=np.array(empty_seats_boxes[-1]).astype(int)
        cv2.rectangle(image, (x1, y1), (x2, y2), (0, 0, 255), 2)
        # Draw middle point of last box
        x_center=int((x1+x2)/2)
        y_center=int((y1+y2)/2)
        cv2.circle(image, (x_center, y_center), 5, (0, 0, 255), -1)
        # Draw the text
        cv2.putText(image, "Empty seat for guest", (x1, y1-10), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)
        return image
    
    def draw_skeleton_on_image(self, image, keypoints):
        # # Define connections in the skeleton
        # connections = [
        #     (0, 1), (1, 2), (2, 3),  # Head
        #     (4, 5), (5, 6), (6, 7),  # Body
        #     (8, 9), (9, 10),         # Left Arm
        #     (8, 11), (11, 12),       # Right Arm
        #     (0, 4), (0, 8)           # Body connections
        # ]

        # Define connections in the skeleton
        # The BODY_38 body format contains 38 keypoints 
        # define in https://www.stereolabs.com/docs/body-tracking/#tabBody38
        # connections_38 = [
        #     (9, 7), (8, 6), (6, 5),(7,5),(5,4),  # Head
        #     (4,3),(3,2),(2,1),(1,0),  # Body
        #     (31, 17), (33,17),(35,17),(37,17),(17,15),(15,13),(13,11),(11,3), # Right Arm
        #     (30,16),(32,16),(34,16),(36,16),(16,14),(14,12),(12,10),(10,3), # Left Arm
        #     (25,23),(27,23),(29,23),(23,21),(21,19),(19,0),  # Right Leg
        #     (24,22),(26,22),(28,22),(22,20),(20,18),(18,0)   # Left Leg
        # ]

        connections_18 = [
            (0, 1),  # Neck to Shoulder Center
            (1, 2), (2, 3), (3, 4),  # Left Arm
            (1, 5), (5, 6), (6, 7),  # Right Arm
            (1, 8), (8, 9), (9, 10),  # Left Leg
            (1, 11), (11, 12), (12, 13),  # Right Leg
            (0, 14), (14, 16),  # Face - Left Eye
            (0, 15), (15, 17)  # Face - Right Eye
        ]

        # Draw keypoints
        for keypoint_object in keypoints:
            for point in keypoint_object:
                x, y = int(point[0]), int(point[1])
                cv2.circle(image, (x, y), 4, (0, 255, 0), -1)

        # Draw connections
        for connection in connections_18:
            for keypoint_object in keypoints:
                start_point = (int(keypoint_object[connection[0]][0]), int(keypoint_object[connection[0]][1]))
                end_point = (int(keypoint_object[connection[1]][0]), int(keypoint_object[connection[1]][1]))
                if any(x <= 0 for x in start_point): continue
                if any(x <= 0 for x in end_point): continue
                cv2.line(image, start_point, end_point, (0, 255, 0), 2)
    
            

def main(args=None):
    rclpy.init(args=args)
    visualization = Visualization()
    rclpy.spin(visualization)
    visualization.destroy_node()
    rclpy.shutdown()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()
